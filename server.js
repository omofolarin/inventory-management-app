/* eslint no-console: 0 */

const path = require('path');
const express = require('express');
const webpack = require('webpack');
const webpackMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');
const config = require('./webpack.config.js');

const isDeveloping = process.env.NODE_ENV !== 'production';
const port = isDeveloping ? 42000 : process.env.PORT;
const app = express();
// console.log(process.env)
if (isDeveloping) {
    const compiler = webpack(config);
    const middleware = webpackMiddleware(compiler, {
        publicPath: config.output.publicPath,
        contentBase: 'src',
        stats: {
            colors: true,
            hash: false,
            timings: true,
            chunks: true,
            chunkModules: true,
            modules: false
        }
  });

    app.use(middleware);
    app.use(webpackHotMiddleware(compiler, {
        'log': false,
        'path': '/__webpack_hmr',
        'heartbeat': 10 * 1000
        }));
    app.get('*', function response(req, res) {
        console.log(path.join(__dirname, 'dist/index.html'));
        // console.log(middleware.fileSystem.readFileSync(path.join(__dirname, 'dist/index.html')));
        res.write(middleware.fileSystem.readFileSync(path.join(__dirname, 'dist/index.html')));
        res.end();
    });
    } else {
        // We basically just create a child process that will run
    // the production bundle command
    var child_process = require('child_process');
    child_process.exec("webpack -p --config webpack.production.config.js", function (error, stdout, stderr) {
        console.log('stdout: ' + stdout);
        console.log('stderr: ' + stderr);
        if (error !== null) {
        console.log('exec error: ' + error);
        }

        app.use(express.static(__dirname + '/dist'));
    app.get('*', function response(req, res) {
        res.sendFile(path.join(__dirname, 'dist/index.html'));
    });
    });

 
}

  app.listen(port, '0.0.0.0', function onStart(err) {
  if (err) {
    console.log(err);
  }
  console.info('==> 🌎 Listening on port %s. Open up http://0.0.0.0:%s/ in your browser.', port, port);
});

