import React, { Component } from 'react'
import AppRoutes from './routes'
import "styles/normalize.scss";


class App extends Component {
    constructor (props) {
    super (props)
  }

    render () {
        return(
            <AppRoutes />
        )
    }
}

export default App;
