import React from 'react'
import styles from './Alert.scss'

const chooseType = (type) => {
    switch (type) {
        case 'info':
            return styles.alertInfo
        case 'warning':
            return styles.alertWarning
        case 'error':
            return styles.alertError
        case 'success':
            return styles.alertSuccess
        default:
            return styles.alertInfo
    }
}

const chooseSize = (size) => {
    switch (size) {
        case 'large':
            return styles.alertLarge
        case 'medium':
            return styles.alertMedium
        case 'small':
            return styles.alertSmall
        default:
            return styles.alertSmall
    }
}

const choosePosition = (position) => {
    switch (position) {
        case 'center':
            return styles.center
        case 'left':
            return styles.left
        case 'right':
            return styles.right
        default:
            return styles.center
    }
}
export const Alert = (props) => {
    const { children, type, size, position, className } = props
    const selectedType = chooseType(type)
    const selectedSize = chooseSize(size)
    const selectedPosition = choosePosition(position)
    return(
        <div className={[styles.alert, selectedType, selectedSize, selectedPosition, className ].join(' ')}>
            <p>{children}</p>
        </div>
    )
}

export default Alert