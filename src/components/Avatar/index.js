import React from 'react'
import PropTypes from 'prop-types'
import styles from './Avartar.scss'

export const Avatar = (props) => {
    const { image, shape, size, className, borderColor, children, backgroundColor, style} = props;
    let avartarShape = shape == 'circle' ? styles.circledAvartar : styles.squaredAvartar;
    let innerStyle = {
        height: size == null ? '5em' : size+'em',
        width: size == null ? '5em' : size+'em',
        borderColor: borderColor ==  null ? '#ccc' :  borderColor,
        borderRadius :  shape == null || shape == 'circle' ? '50%' : '0px',
        backgroundColor: backgroundColor == null ? '#ccc' : backgroundColor
    }
    return(
        <div className={[styles.avartar, className].join(' ')} style={[innerStyle, style]}>
            {children}
        </div>
    )
}

export default Avatar