import React from 'react'
import PropTypes from 'prop-types'
import { Switch, Route } from 'react-router-dom'

/**
 * 
 * Conversation Bots intension is to help a user achieve a specific task
 * Dialog is an instance of the conversation
 * Intent is the task to be achieved
 * HappyPath is a stage in the conversation flow in which the user is succeeding with the task
 * SadPath is a stage in which the user is have trouble in the task
 * lostPath is a path in which the user is taking an unexpected turn in the task to be achieved
 * 
 */

export const BotRoute = (props) => {
    const { routes } = props; 
    const botRoutes = routes.map(( botRoute, i) => {
        console.log(botRoute.exact);
       return <Route path={botRoute.path} component={botRoute.component} key={i} exact={botRoute.exact}/>
    })

    return (
        <div>
            <Switch>
                {
                    routes.map(( botRoute, i) => {
                        // console.log(botRoute);
                    return <Route path={botRoute.path} component={botRoute.component} key={i} />
                    })
                }
            </Switch>
        </div>
    )
}


export const Bot = (props) => {
    const { name, description, children, avatar } = props;
    return (
        <div>
            { children}
        </div>
    )
}

export const Dialog = (props) => {
    const { input, onHappyPath, onSadPath, children } = props
    return(
        <div>
            {children}
        </div>
    )
}

Dialog.proptypes = {
    input: PropTypes.object,
    onHappyPath: PropTypes.func,
    onSadPath: PropTypes.func,

}

export const Intent = (props) => {
    const { 
        title, 
        success, 
        failed, 
        processing, 
        abandoned, 
        completed,
        children,
    } = props;
    return(
        <div>
            {children}
        </div>
    )
}

Intent.proptypes = {
    title: PropTypes.string,
    success: PropTypes.bool,
    failed: PropTypes.bool,
    processing: PropTypes.bool,
    abandoned: PropTypes.bool,
    completed: PropTypes.bool
}

export const HappyPath = (props) => {
    const { stage, children } = props
    return (
        <div>
            {children}
        </div>
    )
}

HappyPath.proptypes = {
    stage: PropTypes.number,
    completionRating: PropTypes.number,
    problemEncounteredRating: PropTypes.number,
}

export const SadPath = (props) => {
    const { stage, completionRating, problemEncounteredRating, children } = props
    return (
        <div>
            {children}
        </div>
    )
}

SadPath.proptypes = {
    stage: PropTypes.number,
    completionRating: PropTypes.number,
    problemEncounteredRating: PropTypes.number
}
export const LostPath = (props) => {
    const { stage, children } = props;
    return (
        <div>
            {children}
        </div>
    )
}
export default Dialog