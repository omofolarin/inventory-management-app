import React from 'react'
import PropTypes from 'prop-types';
import {
  Link
} from 'react-router-dom'
import './Button.scss';
import styles from './Button.scss'
import { ClipLoader } from 'react-spinners';


const chooseType = (type) => {
    switch (type) {
        case 'elevated':
            return styles.elevated;
        case 'flat':
            return styles.flat;
        case 'transparent':
            return styles.transparent;
        case 'fab': 
            return styles.fab;

        case 'neutral':
            return styles.neutral;
        case 'inverted':
            return styles.inverted;
        default:
            return styles.elevated;
    }
}


export const Button = (props) => {
 
    const { onClick, children, className, type, isLoading, style, size, title, icon } = props;
    const selectType = chooseType(type);
    let loadingStyle = isLoading === true ? styles.loadingButton : null;
    let loadingTitleStyle = isLoading ? styles.loadingTitle : null;
    let loading = isLoading == true ? <span className={styles.loader}><ClipLoader color="#ffffff" size={15} /></span> : null
    let placeholder = title != null && type != 'fab' ? 
                        <span className={[styles.title, loadingTitleStyle ].join('  ')}>{title}</span> :
                        <div>{ icon }</div>
    return (
        <button className={[ styles.button, selectType, loadingStyle, className ].join(' ')}  onClick={onClick} style={style}>
            {placeholder}
            {loading}
        </button>
    )
}

export default Button;

