import React, { Component } from 'react'
import styles from './Card.scss'
import { Link } from 'react-router-dom'

export const Card = (props) => {
    let position = null
    props.position == 'right' ? position = styles.cardRight : position = styles.cardLeft 
    return (
        <div className={styles.card + " " + position} style={props.style}>
            {props.children}
         </div>
    )
}

export const CardCenter = props => {
    const { children } = props
    return (
        <div className={styles.cardCenter}>
          {children}
        </div>
    )
}

export const CardNav = (props) => {
    return (
        <div>
            {props.children}
        </div>
    )
}
 
export const CardList = (props) => {
    return (
        <ul className={styles.cardList} >
            {props.children}
        </ul>
    )
}

export const CardListItem = (props) => {
    const { underlineColor, className } = props
    let style = {
        borderColor: underlineColor
    }
    return (
        <li className={[styles.cardListItem, className].join(' ')} style={style}>
            {props.children}
        </li>
    )
}

export const SuscribedJobFeed = (props) => {
    const { jobLocationIsOn } = props
    
    let locationIsOn = '';
    jobLocationIsOn == true ? locationIsOn = styles.JobFeedLocationIsOn : null
    return (
        <div>
            <Avatar size='3' borderColor='#999999' shape="circle" customClass={styles.JobFeedContent}/>
            <div className={styles.JobFeedContent}>
                <div className={styles.JobLFeedText}>Title</div>
                <div className={styles.JobLFeedText}>Company Name</div>
                <div className={styles.JobFeedLocation + ' ' + locationIsOn}><i className={styles.materialIcons}>&#xE0C8;</i></div>
            </div>
        </div>
    )
}

export const FriendList = (props) => {
    return (
        <div className={styles.friendList}>
            {props.children}
        </div>
    )
}

export const Friend = (props) => {
    let borderColor = '#ccc', friendisActive = ''
    const {isActive} = props
    if (isActive) {
        borderColor = '#3C70AD'
        friendisActive = styles.friendIsActive
    }    
    return (
        <div className={styles.friend}>
            <Avatar shape="circle"borderColor={borderColor} size="2.5" customClass={styles.friendAvatar}/>
            <div className={styles.friendDetails + ' ' + friendisActive}>
                
            </div>
        </div>
    )
}
//<Link to="\">{props.itemName}</Link>

// export const CardListItem = (props) => {
//     return(
//         <li className={styles.CardNotificationListItem}>
//             <Link to="\">
//                  <div>  
//                     <Avatar shape="circle" customClass={styles.contentAvatar} />
//                     <div className={styles.contentDetails}>
//                   <span className={styles.text}>{props.title}</span>
//                     <div className={styles.text}>{props.companyName}</div>
//                     </div>
//                 </div>   
//             </Link>
//         </li>

//     )
// }

export const CardFriendListItem = (props) => {
    let isActive = ''
    if (props.active == true ) { isActive = styles.CardFriendListItemisActive  }
    return (
    <li className={styles.CardFriendListItem +  ' ' + isActive }>
            <Link to="\">
                 <div>  
                    <Avatar shape="circle" customClass={styles.contentAvatar} />
                    <div className={styles.contentDetails} style={{height: '2.7em'}}>
                    <span className={styles.title}>{props.name}</span>
                    </div>
                </div>   
            </Link>
        </li>
    )
}

// export const CardList = (props) => {
//     return(
//         <div> 
//         <ul className={styles.CardNotificationList}>
//             {props.children}
//         </ul>
//          <span className={styles.CardViewAll}>View All</span>
//         </div>
//     )
// }

