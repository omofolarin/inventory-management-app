import React, {Component} from 'react'
import PropTypes from 'prop-types';


const Content = (props) => (

        <div className="o-content__body l-content__body">
          <div className="o-body__title">
            <h1 className="heading">Latest cakes</h1>
          </div>
          { props.children}
        </div>
);

export default Content
