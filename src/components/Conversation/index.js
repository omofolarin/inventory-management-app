import React from 'react'
import styles from './conversation.scss'

export const Conversation = (props) => {
    return(
        <div>
            {props.children}
        </div>
    )
}


export const MessageTo = (props) => {
    return(
        <div>
            {props.children}
        </div>
    )
}


export const MessageFrom = (props) => {
    const { text } = props; 
    return(
        <div  className={styles.conversationP}><p className={styles.conversation}> {text}</p></div>
    )
}

export default Conversation