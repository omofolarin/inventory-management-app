import React, { Component}  from 'react';
import PropTypes from 'prop-types';
import { MdSearch } from 'react-icons/lib/md';
import Select from 'react-select';
import Dropzone from 'react-dropzone'
import VirtualizedSelect from 'react-virtualized-select';
import '!style-loader!css-loader!react-select/dist/react-select.css';
import '!style-loader!css-loader!react-virtualized/styles.css'
import '!style-loader!css-loader!react-virtualized-select/styles.css'

import styles from './Input.scss';

export const SearchInput = (props) => {
    const { input, meta, className, label, required } = props;
    const { name, value, onBlur, onChange, onFocus } = input;
    const { active, error, submitting, touched, dirty, autofilled, visited } = meta;
    
    return(
        <div>
            <div className={[styles.inputFieldGroup, className].join(' ')}>
                <label htmlFor={name} className={styles.inputLabel}>{label}</label>
                <MdSearch size={30} color="gray" className={styles.searchIcon} />
                <input type="search" name={name} id="text" className={[styles.inputField, styles.searchInput ].join(' ')} value={value} onChange={onChange} required={required} />
                { (touched || visited ) && error && <div className={styles.inputErrorMessage}>{error}</div>}
                 <div className={styles.inputSuccessMessage}>

                </div>
            </div>
        </div>
    )
};


export const TextInput = (props) => {
    const { input, meta, className, label, required } = props;
    const { name, value, onBlur, onChange, onFocus } = input;
    const { active, error, submitting, touched, dirty, autofilled, visited } = meta;
    
    return(
        <div>
            <div className={[styles.inputFieldGroup, className].join(' ')}>
                <label htmlFor={name} className={styles.inputLabel}>{label}</label>
                <input type="text" name={name} id="text" className={styles.inputField} value={value} onChange={onChange} required={required} />
                { (touched || visited ) && error && <div className={styles.inputErrorMessage}>{error}</div>}
                 <div className={styles.inputSuccessMessage}>

                </div>
            </div>
        </div>
    )
};

export class SuggestibleInput extends Component {
    render() {
        const { input, meta, className, label, suggestionList, selectedItem, showDropdown, hideDropdown, isDropdownActive, onClickSuggestion } = this.props;
        const { name, value, onBlur, onChange, onFocus } = input;
        const { active, error, submitting, touched, dirty, autofilled, visited } = meta;
        let getValue = '';
        let suggestionIndexes = []
        
        let suggestions = suggestionList.map((suggestion, i) => {
            const valueRegex = new RegExp(value, "gi");
            let match = suggestion.name.match(valueRegex)
            // return <div className={styles.suggestionItem} key={i} onClick={()=> onClickSuggestion(suggestion.name)} >{suggestion.name}</div>
            
            if ( match != null && value.length > 2) {
                suggestionIndexes.push(i);
                // return <div className={styles.suggestionItem} key={i} onClick={()=> onClickSuggestion(suggestion.name)} >{suggestion.name}</div>
            } 
            return null;
        });
    
        if (suggestionIndexes.length != 0 ) {
            showDropdown();
        }
    
        const suggestionDropdown = isDropdownActive == true ? suggestions : null;
    
        return (
            <div>
                <div className={[styles.inputFieldGroup, className].join(' ')}>
                    { (touched || visited ) && error && <div className={styles.inputErrorMessage}>{error}</div>}
                    <div className={styles.inputSuccessMessage}>
    
                    </div>
                    <label htmlFor={name} className={styles.inputLabel}>{label}</label>
                    <input type="text" name={name}  className={styles.inputField} onChange={onChange}  autoComplete="off" value={value}/>
                    <div className={styles.suggestions}>
                        <div className={styles.suggestionsList}>
                        {suggestions}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}    

export const Input = (props) => {
    const { input, meta, className, label} = props;
    const { name, value, onBlur, onChange, onFocus } = input;
    const { active, error, submitting, touched, dirty, autofilled, visited } = meta;
    
    return(
        <div>
            <div className={[styles.inputFieldGroup, className].join(' ')}>
                <label htmlFor={name} className={styles.inputLabel}>{label}</label>
                <input type="text" name={name} id="text" className={styles.inputField} value={value} onChange={onChange}/>
                { (touched || visited ) && error && <div className={styles.inputErrorMessage}>{error}</div>}
                 <div className={styles.inputSuccessMessage}>

                </div>
            </div>
        </div>
    )
}


export const Radiobox = (props) => {
    const { input, meta, className, label} = props;
    const { name, value, onBlur, onChange, onFocus } = input;
    const { active, error, submitting, touched, dirty, autofilled } = meta;
    return(
        <div style={{}}>
             <div className={[styles.radioFieldGroup, className].join(' ')}>
                <label htmlFor={name} className={styles.radioLabel}>{label}</label>
                <input type="radio" name={name} id={name} className={styles.radioField} value={value} onChange={onChange}/>
                <div className={styles.inputErrorMessage}>

                </div>
                 <div className={styles.inputSuccessMessage}>

                </div>
            </div>
        </div>
    )
}

export const EmailInput = (props) => {
    const { input, meta, className, label} = props;
    const { name, value, onBlur, onChange, onFocus } = input;
    const { active, error, submitting, touched, dirty, autofilled } = meta;
   
    return(
        <div>
            <div className={[styles.inputFieldGroup, className].join(' ')}>
                <label htmlFor={name} className={styles.inputLabel}>{label}</label>
                <input type="email" name={name} id="email" className={styles.inputField} value={value} onChange={onChange}/>
                { touched && error && <div className={styles.inputErrorMessage}>{error}</div>}   
                 <div className={styles.inputSuccessMessage}>

                </div>
            </div>
        </div>
    )
}


export const NumberInput = (props) => {
    const { input, meta, className, label} = props;
    const { name, value, onBlur, onChange, onFocus } = input;
    const { active, error, submitting, touched, dirty, autofilled } = meta; 
    return(
        <div>
            <div className={[styles.inputArea, className].join(' ')}>
                <label htmlFor={name} className={styles.inputLabel}>{label}</label>
                <input type="number" name={name} id={name} className={styles.inputField} value={value} onChange={onChange}/>
                { touched && error && <div className={styles.inputErrorMessage}>{error}</div>}                
                 <div className={styles.inputSuccessMessage}>

                </div>
            </div>
        </div>
    )
}

export const PasswordInput= (props) => {
    const { input, meta, className, label} = props;
    const { name, value, onBlur, onChange, onFocus } = input;
    const { active, error, submitting, touched, dirty, autofilled } = meta;
    
    return(
        <div>
            <div className={[styles.inputFieldGroup, className].join(' ')}>
                <label htmlFor={name} className={styles.inputLabel}>{label}</label>
                <input type="password" name={name} id={name} className={styles.inputField} value={value} onChange={onChange}/>

                { touched && error && <div className={styles.inputErrorMessage}>{error}</div>}
                 <div className={styles.inputSuccessMessage}>

                </div>
            </div>
        </div>
    )
}

export const Textarea = (props) => {
   const { input, meta, className, label} = props;
    const { name, value, onBlur, onChange, onFocus } = input;
    const { active, error, submitting, touched, dirty, autofilled } = meta; 
    return(
        <div>
            <div className={[styles.inputArea, className].join(' ')}>
                <label htmlFor={name} className={styles.InputLabel}>{label}</label>
                <textarea className={styles.InputField}>

                </textarea>
                <div className={styles.inputErrorMessage}>

                </div>
                 <div className={styles.inputSuccessMessage}>

                </div>
            </div>
        </div>
    )
}

export const SearchableSelect = (props) => {
    const { input, name, options, label, meta: { touched, error }, children, className, ...custom} = props;

    return (
        // <div className={[styles.inputFieldGroup, className].join(' ')} >
            <Select 
                name={name}
                value={input.value}
                onChange={(e, index, value) => input.onChange(value)}
                options={options}
                className={styles.inputFieldGroup}
                {...input}
                {...custom}
            />
        // </div>
    )
};

export const VirtualizedSearchableSelect = (props) => {
    const { input, name, options, label, meta: { touched, error }, children, className, ...custom} = props;

    console.log(input.value.value);
    return (
        <div>
            <div className={[styles.inputFieldGroup, styles.virtualizedSearchSelect, className].join(' ')} >
                <label htmlFor={name} className={styles.InputLabel}>{label}</label>
                <VirtualizedSelect 
                    { ...input }
                    { ...custom }
                    name={name}
                    value={input.value || ''} 
                    onChange={(values) => input.onChange(values)} 
                    onBlur={() => input.onBlur(input.value)}
                    options={options}
                />
                <div className={styles.inputErrorMessage}>

                </div>
                <div className={styles.inputSuccessMessage}>

                </div>
            </div>
        </div>
       
    )
}

export const FileInput = (props) => {
    const { input, meta, className, label, children } = props;
    const { name, value, onBlur, onChange, onFocus } = input;
    const { active, error, submitting, touched, dirty, autofilled, visited } = meta;
    const files = input.value;
    
    return(
        <section>
            <div className="dropzone" >
            <Dropzone 
                onDrop={( filesToUpload, e ) => input.onChange(filesToUpload)} 
                name={name}
                style={{marginLeft: 'auto', marginRight: 'auto', width: '50%', background: '#f2f2f2'}}
            >
            { children}
            </Dropzone>
            </div>
            <aside>
                {
                    meta.touched &&
                    meta.error &&
                    <span className="error">{meta.error}</span>
                }
                    <h2>Dropped files</h2>
                {
                    files && Array.isArray(files) && (
                        <ul>
                        { files.map((file, i) => <li key={i}>{file.name}</li>) }
                        </ul>
                    )
                }
            </aside>
        </section>
    )
}
