import React, {Component} from 'react'
import PropTypes from 'prop-types';
import {
  Link
} from 'react-router-dom'
import styles from './Header.scss'


export const Header = (props) => {
  return (
    <header className={styles.header}>
      {props.children}
    </header>
  )
}

export const HeaderNavBtn = (props) => (
    <div className={styles.headerNavButton} onClick={props.onClick}>
        <i className={styles.materialIcons}>&#xE5D2;</i>
        <hello/>
        <edit></edit>
    </div>
)

export const HeaderNavBtnIcon = (props) => {
   let active = null
   if (props.isToggledNav == true) {
     active = styles.headerNavButtonIconActive
   }
    return(
    <div className={styles.headerNavButtonIcon + ' ' + active} onClick={props.onClick}>
        <i className={styles.materialIcons}>&#xE5D2;</i>
    </div>
    )
}


export const HeaderTitle = (props) => (
    <div className={styles.headerTitle}>
        <Link to="/"><span>{props.title}</span></Link>
    </div>
)


export const HeaderSearchButton = (props) => (
  <div className={styles.headerSearchButton} onClick={props.onClick}>
      <i className={styles.materialIcons}>search</i>
  </div>
)


export const HeaderSearchBox = (props) => {
    let activateSearch = null;
    let showSearchBoxCancel = null;
    let showSearchBoxButton = null;

    if ( props.isHit == true) {
        activateSearch = styles.searchActive;
        showSearchBoxCancel = styles.searchBoxCancelVisible
        showSearchBoxButton = styles.earchBoxButtonVisible
    }

  return (
    <div className={styles.headerSearchBox + " " + props.className}>
        <form>
            <input type="search" name="" value="" className={[styles.searchBoxInput, activateSearch].join(' ')} placeholder={props.placeHolder} value={props.value} onClick={props.onClick} onChange={props.onChange}/>
                <div className={styles.searchBoxIcon}>
                    <i className={styles.materialIcons}>&#xE8B6;</i>
                </div>
            <button className={[styles.searchBoxButton, showSearchBoxButton].join(' ') }>search</button>
        </form>
        <div className={[styles.searchBoxCancel, showSearchBoxCancel].join(' ')} onClick={props.onReturnSearchBar}>
            <i className={styles.materialIcons}>&#xE317;</i>
        </div>
    </div>
  )
}


export const HeaderUserSearchBox = (props) => {
    let activateSearch = null;

    if ( props.isHit == true) {
    activateSearch = styles.headerUserSearchBoxisActive;
    }
    return (
    <div>
        <div className={styles.headerUserSearchBox + ' ' + activateSearch}>
            <div className={styles.UsersearchBoxIcon}>
                <i className={styles.materialIcons}>&#xE8B6;</i>
            </div>
            <input type="search" name="" value="" className={styles.headerUserSearchBoxInput} placeholder={props.placeHolder} value={props.value} onClick={props.onClick} onChange={props.onChange}/>
            <div className={styles.UsersearchBoxCancel} onClick={props.onReturnSearchBar}>
                <i className={styles.materialIcons}>&#xE14C;</i>
            </div>
        </div>
            {props.children}
    </div>
    )
}

export const SearchBoxDropDown = (props) => {
    let active = null
    if(props.isHit == true) {
        active = styles.searchBoxDropDownisActive
    }
  return (
    <div className={styles.searchBoxDropDown + ' ' + active}>
       Search Results
    </div>
  )
}

const navMenuType = (type, attributes) => {
    const { icon, title} = attributes
    switch (type = type) {
        case 'icons':
            return (
                <i className={[styles.materialIcons, styles.headerNavIcon].join(' ')}>{icon}</i>
            )
        case 'text':
            return <span className={styles.headerNavText}>{title}</span>
        case 'textIcons':
            return ( <span>
                        <span className={styles.headerNavText}>{title}</span>
                        <span className={[styles.materialIcons ,styles.headerNavIcon].join(' ')}>
                            {iconTag}
                        </span>
                    </span>
            )
        case 'IconText':
            return (    
                <span>
                    <span><i className={styles.materialIcons}>{icon}</i></span>
                    <span>{title}</span>
                </span>
            )
        default:
            return (
                <i className={styles.materialIcons}>{icon}</i>
            )
        
    }
}

export const HeaderNavMenu = (props) => {
    const { isToggledNav, onToggleOff, navRoutes, privateRoutes, listType, authSuccess } = props
    let showNavMenu = null
    if (isToggledNav == true) {
        showNavMenu = styles.headerNavShow;
    }

    const navMenues = navRoutes.map((route) => {
        let label = navMenuType(listType, route)
        return (
            <div className={styles.headerNavListItem} key={route.id}>
                <Link to={route.path} onClick={onToggleOff}> {label} </Link>
            </div>
        )
    })

    // const privateMenues = privateRoutes.map((route) => {
    //     let label = navMenuType(listType, route)
    //     return (
    //         <div className={styles.headerNavListItem} key={route.id}>
    //             <Link to={route.path} onClick={onToggleOff}> {label} </Link>
    //         </div>
    //     )
    // })

  /*
    
   Todo: ensure that navItem is not more than 4, if more is to be added it must be a dropdown from a more/options symbol.
  */
  return (
    <div className={[styles.headerNav, styles.showNavMenu].join(' ')}>
        <div className={styles.headerNavlist}>
            {navMenues}
        </div>
        <div className={styles.headerNavlistOverlay} onClick={props.onToggleOff}>

        </div>
    </div>
  )
}

export const HeaderNavIconMenu = (props) => {
  let showNavMenu = null;
  if (props.isToggledNav == true) {
      showNavMenu = styles.headerNavIconShow;
  }
  if (props.isHitSearch == true){
      showNavMenu = null 
    }
  return (
    <div className={styles.headerNavIcon + ' ' + showNavMenu}>
        {props.children}
    </div>
  )
}

export const HeaderNavIconMenuSecondary= (props) => {
    return (
    <div className={styles.headerNavIconSecondary}>
        {props.children}
    </div>
  )
}

export const HeaderNavContent = (props) => {
    let navContentisActive = null
    if(props.id == props.activeId) {
        navContentisActive = styles.headerNavContentisActive
    }
   // props.activeId === props.id ? navContentisActive = styles.headerNavContentisActive : navContentisActive = null
   // actions.id == props.id ? console.log(true) : console.log(false)
  // props.isNotificationActive ? navContentisActive = styles.headerNavContentisActive : navContentisActive = null
    
    return (
        <div className={styles.headerNavContent + ' ' + navContentisActive}>
            {props.children}
        </div>
    )
}

export const HeaderNavItem = (props) => {
  let {icon , path , type ,text, customClass} = props

    return (
      <div style={{position: 'relative'}}>
        <div className={styles.headerNavItem + ' ' + customClass}>
          <Link to={ path } onClick={props.onClick}>
               { type == text ? <span>{text}</span> : <i className={styles.materialIcons}>{icon}</i> }
                 
          </Link>
            {props.children}
        </div>

      </div>
    )
}


export const SearchPage = (props) => {
    let searchBoxBody = null ;
    if (props.isHitSearch == true) {
        searchBoxBody = styles.searchBoxBodyVisible;
    }
    return (
        <div className={[styles.searchBoxBody, searchBoxBody].join(' ')}>
            {props.children}
        </div>
    )
}

export const SearchSuggestions = (props) => {
    return (
        <div className={styles.searchBoxSuggestionsList}></div>
    )
}

export const NavItemDropdown = (props) => {
    return(
        <div className={styles.headerNavListItem}>

        </div>
    )
}

Header.propTypes = {
    children: PropTypes.array,
    handleToggleNav: PropTypes.func,
    handleHitSearch: PropTypes.func,
    handleToggleNavOff: PropTypes.func,
    handleReturnSearchBar: PropTypes.func,
    handleSearchInputChange: PropTypes.func
}

HeaderTitle.propTypes = {
    title: PropTypes.string
}

HeaderSearchBox.propTypes = {
    placeHolder: PropTypes.string,
    value: PropTypes.string,
    onClick: PropTypes.func,
    isHit: PropTypes.bool,
    onChange: PropTypes.func,
    onReturnSearchBar: PropTypes.func
}

HeaderNavBtn.propTypes = {
    onClick: PropTypes.func,
    isToggled: PropTypes.bool
}

HeaderNavMenu.propTypes = {
    isToggledNav: PropTypes.bool,
    onToggleOff: PropTypes.func
}

SearchPage.propTypes = {
    isHitSearch: PropTypes.bool,
    children: PropTypes.array
}
