import React , { Component } from 'react'
import styles from './Label.scss'

export const NotificationLabel = (props) => {
  let { indicator } = props;
  let isActive = true
  let labelShow = null
  isActive ? labelShow = styles.notificationLabelisActive : labelShow = null
  return (
    <div className={styles.notificationLabel + ' ' + labelShow } onClick={props.onClick}>
      <span>{indicator}</span>
    </div>
  
  )
}
