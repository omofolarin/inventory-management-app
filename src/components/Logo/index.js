import React from 'react'
import styles from './Logo.scss'

export const Logo = (props) => {
    const { className } = props;
    return(
        <div  className={[styles.logoBorderStyle, className].join(' ')}>
            <img src={require('images/logo.png')} style={{padding: '0.5em', paddingLeft: '0.45em', paddingTop: '0.45em', width: '60%'}}/>
        </div>
    )
}

export default Logo