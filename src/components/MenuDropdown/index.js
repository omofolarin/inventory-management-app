import React from 'react';
import styles from './MenuDropdown.scss'


export const MenuDropdown = (props) => {
    const { children, className, style } = props;
    return (
        <div className={[styles.menuDropdown, className].join(' ')} style={style}>
            {children}
        </div>
    )
}

export default MenuDropdown

