import React from 'react'
import styles from './Modal.scss'

export const Modal = (props) => {
    const { isActive } = props;
    let display = <div></div>;
    if (isActive) {
      display =  <div>
            <div className={styles.modal}></div>
                {props.children}
        </div>
    } else {
        display = null;
    }
    return(
       <div>{display}</div>
       
    )
}

const chooseModalSize = (size) => {
     switch (size) {
        case 'small':
            return styles.modalSmall
        case 'medium':
            return styles.modalMedium
        case 'large': 
            return styles.modalLarge
        default:
            return styles.modalMedium
    }
}

export const ModalContent = (props) => {
    const { size, children, style } = props;
    const modalSize = chooseModalSize(size)

    return (
        <div className={modalSize} style={style}>
            {children}
        </div>
    )
}