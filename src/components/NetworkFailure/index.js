import React from 'react'


export const NetworkFailure = (props) => {
    return(
        <div>
            {props.children}
        </div>
    )
}

export default NetworkFailure