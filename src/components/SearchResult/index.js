import React, { Component } from 'react'
import styles from './SearchResult.scss'
import Avatar from 'react-toolbox/lib/avatar';
import {Button, IconButton} from 'react-toolbox/lib/button';


export const SearchResult = (props) => {
    return (
        <div className={styles.searchPage} >
            <div className={styles.searchContent}>
                <div className={styles.searchResultCaption}>
                    <p>Results:</p>
                </div>
                <div className={styles.searchResultBody}>
                    <div className={styles.searchResultItem}>
                        <div className={styles.resultAction}>
                            <div className={styles.avartar}></div>
                            <Button  label='Apply' flat primary raised className={styles.resultActionButton}/>
                        </div>
                        <div className={styles.resultContent}>
                            <p><h5 className={styles.tagHeading}>Title:</h5></p>
                            <p><h5 className={styles.tagHeading}>Company Name:</h5></p>
                            <div className={styles.inlineTagHeadings}>
                              <p className={styles.inlineTag}><h5 className={styles.tagHeading}>Location:</h5></p>
                              <p className={styles.inlineTag}><h5 className={styles.tagHeading}>Salary:</h5></p>
                              <p className={styles.inlineTag}><h5 className={styles.tagHeading}>Employment Type: </h5></p>
                            </div>
                            <div className={styles.inlineTagHeadings}>
                                <p className={styles.inlineTag}><h5 className={styles.tagHeading}>Posted:</h5></p>
                                <p className={styles.inlineTag}><h5 className={styles.tagHeading}>Valid Through: </h5></p>
                            </div>
                            <p><h5 className={styles.tagHeading}>Description: </h5></p>
                        </div>   
                    </div>
                     <div className={styles.searchResultItem}>
                        <div className={styles.resultAction}>
                            <div className={styles.avartar}></div>
                            <Button  label='Apply' flat primary raised className={styles.resultActionButton}/>
                        </div>
                        <div className={styles.resultContent}>
                            <p><h5 className={styles.tagHeading}>Title:</h5></p>
                            <p><h5 className={styles.tagHeading}>Company Name:</h5></p>
                            <div className={styles.inlineTagHeadings}>
                              <p className={styles.inlineTag}><h5 className={styles.tagHeading}>Location:</h5></p>
                              <p className={styles.inlineTag}><h5 className={styles.tagHeading}>Salary:</h5></p>
                              <p className={styles.inlineTag}><h5 className={styles.tagHeading}>Employment Type: </h5></p>
                            </div>
                            <div className={styles.inlineTagHeadings}>
                                <p className={styles.inlineTag}><h5 className={styles.tagHeading}>Posted:</h5></p>
                                <p className={styles.inlineTag}><h5 className={styles.tagHeading}>Valid Through: </h5></p>
                            </div>
                            <p><h5 className={styles.tagHeading}>Description: </h5></p>
                        </div>   
                       
                     </div>
                      <div className={styles.searchResultItem}>
                        <div className={styles.resultAction}>
                            <div className={styles.avartar}></div>
                            <Button  label='Apply' flat primary raised className={styles.resultActionButton}/>
                        </div>
                        <div className={styles.resultContent}>
                            <p><h5 className={styles.tagHeading}>Title:</h5></p>
                            <p><h5 className={styles.tagHeading}>Company Name:</h5></p>
                            <div className={styles.inlineTagHeadings}>
                              <p className={styles.inlineTag}><h5 className={styles.tagHeading}>Location:</h5></p>
                              <p className={styles.inlineTag}><h5 className={styles.tagHeading}>Salary:</h5></p>
                              <p className={styles.inlineTag}><h5 className={styles.tagHeading}>Employment Type: </h5></p>
                            </div>
                            <div className={styles.inlineTagHeadings}>
                                <p className={styles.inlineTag}><h5 className={styles.tagHeading}>Posted:</h5></p>
                                <p className={styles.inlineTag}><h5 className={styles.tagHeading}>Valid Through: </h5></p>
                            </div>
                            <p><h5 className={styles.tagHeading}>Description: </h5></p>
                        </div>   
                       
                      </div>
                </div>
            </div>
        </div>
    )
}