import React from 'react'
import PropTypes from 'prop-types'
import styles from  './Sidebar.scss'
import { Link } from 'react-router-dom'

export const Sidebar = (props) => {
    const { children, isMinimized } = props;
    const minimizeStyle= isMinimized ? styles.sidebarMinimized : null;
    return(
        <div className={[styles.sidebar, minimizeStyle ].join(' ')}>
            {props.children}
        </div>
    )
}

export const SidebarBody = (props) => {
    const { children, isMinimized } = props;
    let minimizeStyle = isMinimized ? styles.sidebarBodyMinimized : null
    return (
        <div className={[styles.sidebarBody, minimizeStyle ].join(' ')}>
            {children}
        </div>
    )
}

export const SidebarTitle = (props) => {
    const { title, url, isMinimized } = props;
    const showTitle = isMinimized ? <span className={styles.sidebarLogo}><img src={require('images/logo-medium.png')} alt="logo"/></span> : <span>{title}</span>
    return (
        <span className={styles.sidebarTitle}>
            <Link to={url} id={styles.sidebarTitleLink}> {showTitle}</Link>
        </span>
    )
}

export const SidebarNavToggle = (props) => {
    const { onClick } = props;

    return (
        <button className={styles.sidebarNavToggle} onClick={onClick}>
            <i className={styles.materialIcons}> &#xE5D2;</i>
        </button>
    )
}

export const SidebarHeader = (props) => {
    const { children } = props;

    return (
        <div className={styles.sidebarHeader}>
            {children}
        </div>
    )
}

export const SidebarNav = (props) => {
    const { navLists, routeMatch, isMinimized } = props;
    const navList = navLists.map((navItem) => {
    let minimizeTitle = isMinimized ?  styles.sidebarItemMinimized : null

        return (
            <li className={styles.sidebarItem} key={navItem.id.toString()}>
                <Link className={styles.sidebarItemLink} to={routeMatch.url + navItem.path}>
                    { navItem.icon }
                    <span className={[styles.sidebarItemTitle, minimizeTitle].join(' ')}>
                            { navItem.title } 
                    </span>
                </Link>
            </li>
        )
    })

    return (
        <div className={styles.sidebarNav}>
            <ul className={styles.sidebarList}>
                {navList}
            </ul>
        </div>
    )
}