import React from 'react'
import PropTypes from 'prop-types'
import styles from  './Sidebar.scss'
import { Link, NavLink } from 'react-router-dom'

export const Sidebar = (props) => {
    const { children } = props;

    return(
        <div className={styles.sidebar}>
            {props.children}
        </div>
    )
}

export const SidebarBody = (props) => {
    const { children } = props;

    return (
        <div className={styles.sidebarBody}>
            {children}
        </div>
    )
}

export const SidebarTitle = (props) => {
    const { title, url } = props;

    return (
        <span className={styles.sidebarTitle}>
            <Link to={url} id={styles.sidebarTitleLink}> {title}</Link>
        </span>
    )
}

export const SidebarNavToggle = (props) => {
    const { toggle, onClick } = props;

    return (
        <button className={styles.sidebarNavToggle}>
            <i className={styles.materialIcons}> &#xE5D2;</i>
        </button>
    )
}

export const SidebarHeader = (props) => {
    const { children } = props;

    return (
        <div className={styles.sidebarHeader}>
            {children}
        </div>
    )
}

export const SidebarNav = (props) => {
    const { navLists } = props;
    const navList = navLists.map((navItem) => {

        return (
            <li className={styles.sidebarItem} key={navItem.id.toString()}>
                <NavLink className={styles.sidebarItemLink} to={navItem.path} activeClassName={styles.sidebarItemLinkActive}>
                    <i className={[styles.sidebarItemIcons, styles.materialIcons ].join(' ')}>
                        { navItem.icon }
                    </i>

                    <span className={styles.sidebarItemTitle}>
                            { navItem.title } 
                    </span>
                </NavLink>
            </li>
        )
    })

    return (
        <div className={styles.sidebarNav}>
            <ul className={styles.sidebarList}>
                {navList}
            </ul>
        </div>
    )
}