import React from 'react'
import PropTypes from 'prop-types'
import styles from './spinner.scss'

export const DualRingSpinner = (props) => {
    return(
        <div className={styles.ldsCss}>
            <div style={{width: '100%',height: '100%'}} className={styles.ldsDualRing}>
                <div></div>
            </div>
        </div>
    )
}

// export default Spinner