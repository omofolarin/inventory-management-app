import React from 'react'
import styles from './Tab.scss'
import { Link, NavLink } from 'react-router-dom'

export const Tab = (props) => {

    return (
        <div style={{float: 'left', width: '100%'}}>
            {props.children}    
        </div>
    )
}

export const TabNav = (props) => {
    return (
         <div className={styles.tab}>
            {/* <div className={styles.tabNav}>
                <NavLink to='/user/home/chat' className={[styles.tabLink].join(' ')} activeClassName={styles.tabLinkActive}>Chat</NavLink>
            </div> */}
            <div className={styles.tabNav}><NavLink to='/user/home/stories' className={styles.tabLink} activeClassName={styles.tabLinkActive } >Stories</NavLink></div>
        </div>
    )
}

export const TabContent = (props) => {
    return (
        <div className={styles.tabContent} style={{overflow: 'scroll', height: '180em', position: 'relative', width: '100%'}}>
            {props.children}
        </div>
    )
}

