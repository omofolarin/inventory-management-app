import React from 'react'
import PropTypes from 'prop-types'
import styles from './TabNav.scss'
import { Link, NavLink } from 'react-router-dom'

export const TabNav = (props) => {
    const { style } = props;
    
    return(
        <div className={styles.tabNav} style={style}>
            {props.children}
        </div>
    )
}

export const TabNavList = (props) => {
    const { tabList, routeMatch } = props
    const tabs = tabList.map((tab) => {
       return (
            <li className={styles.tab} key={tab.id}>
               <NavLink to={ tab.path} className={styles.tabLink} activeStyle={{borderBottom: '3px solid orange'}}>
                    <span className={styles.tabTitle}>{tab.title}</span>
                    <i className={[styles.materialIcons, styles.tabIcons].join(' ')}>{tab.icon}</i>
                </NavLink>
            </li>
       );
    })
    return (
        <ul className={styles.tabNavList}>
            {tabs}
        </ul>
    )
}

export const TabSearch = (props) => {
    const { style } = props;
    return (
        <div className={styles.searchBox} style={style} >
            <input type="search" className={styles.searchInput} />
             <i className={[styles.materialIcons, styles.tabSearchIcon ].join(' ')}>search</i>
        </div>
    )
}

export const TabContent = (props) => {
    const { style } = props;
    return (
        <div className={styles.tabContent} style={style}>
            {props.children}
        </div>
    )
}

/**
 * 
 * Tab content
 * Tab nav
 * 
 * <tab>
 * <tabNav> 
 *  <tabNavList/>
 *  <tabSearch />
 * </tabNav>
 * <tabContent
 * </tab>
 * 
 */
