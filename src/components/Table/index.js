import React from 'react'
import PropTypes from 'prop-types'
import styles from './Table.scss'
import { Link } from 'react-router-dom'
import { MdMoreVert, MdKeyboardArrowLeft, MdKeyboardArrowRight, MdDelete, MdEdit } from 'react-icons/lib/md/'
import MenuDropdown from 'components/MenuDropdown/'


export const Table = (props) => {
    const { className, style } = props;
    return (
        <div className={[styles.table, className].join(' ')} style={style}>
            {props.children}
        </div>
    )
}

export const TableHeader = (props) => { 
    const { className } = props;
    return (
        <div className={[styles.header, className].join(' ')} >
            {props.children}
        </div>
    )
}

export const TableRow = (props) => {
    const { className, children } = props;
    return (
        <div className={[styles.row, className ].join(' ')} >
            {props.children}
            <div className={styles.buttonContainer} >
               <MdMoreVert size={18} color='gray' />
               {/* <MenuDropdown style={{left: '-9em'}}>
                   <div style={{display: 'inline-flex'}}>
                        <div style={{float: 'left', padding: '0.7em', marginRight: '5px' }}><span style={{display: 'inline'}}><MdEdit color="#cccccc" size={15} style={{position: 'relative', top: '-0.5px', float: 'left'}}/><span style={{float: 'right', fontSize: '10px', fontFamily: 'Roboto Regular'}}>Edit</span></span></div>
                        <div style={{float: 'right', padding: '0.7em'}}><span style={{display: 'inline'}}><MdDelete color="#cccccc" size={15} style={{position: 'relative', top: '-0.5px', float: 'left'}} /><span style={{float: 'right', fontSize: '10px', fontFamily: 'Roboto Regular'}}>Delete</span></span></div>
                   </div>
               </MenuDropdown> */}
            </div>
        </div>
    )
}

export const TableColumn = (props) => {
    const { className } = props;
    return (
        <div className={[styles.column, className ].join(' ')} >
             {props.content}
        </div>
    )
}

export const TablePagination = (props) => {
    const { 
        totalPage, 
        nextPage, 
        previousPage, 
        currentPage, 
        onNext, 
        onPrevious, 
        className 
    } = props;

    return (
        <div className={[styles.pagination, className ].join(' ')}>
            <div className={styles.row} style={{paddingTop: '3px', paddingBottom: '3px'}}>
                    <div style={{marginTop: '0.25em'}}> <MdKeyboardArrowLeft size={20} color="#cccccc"/> </div>
                    <div className={styles.paginationIndex}>1</div>
                    <div className={styles.paginationIndex}>2</div>
                    <div className={styles.paginationIndex}>3</div>
                    <div className={styles.paginationIndex}>4</div>
                    <div className={styles.paginationIndex}>5</div>
                    <div style={{marginTop: '0.25em'}} ><MdKeyboardArrowRight size={20} color="#cccccc" /></div>
            </div>
        </div>
    )
}


