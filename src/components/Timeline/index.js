import React , { Component } from 'react'
import Avatar from '../Avatar'
import { Link } from 'react-router-dom'
import styles from './Timeline.scss'

export const Timeline = props => {
    const { children} = props
    return (
        <div className={styles.timelineContainer}>
            {children}
        </div>
    )
}

export const TimelinePostForm = props => {
    return (
        <div className={styles.postForm}>
            <form>
                <textarea className={styles.postFormInput} placeholder="...say something"></textarea>
                <div className={styles.postFormInputBottom}> 
                    <div className={styles.postFormBottomLeft}>
                        <button className={styles.tagButton}>event</button>
                        <button className={styles.tagButton}>history</button>
                        <button className={styles.tagButton}>journal</button>  
                    </div>
                    <div className={styles.postFormBottomRight}>
                        <button className={styles.postFormImgButton}><i className={styles.materialIcons} style={{fontSize: '30px', color: 'gray'}} >&#xE3F4;</i></button> 
                        <input className={styles.postFormButton} type="submit" value="Post Story"/>
                    </div>
                </div>
            </form>
        </div>  
    )
}

export const TimelineFeed = (props) => {
    return (
         <div className={styles.feedContainer}>
             <Avatar size='3' shape="circle" borderColor='#999999' customClass={styles.postAvatar} />
                 <div className={styles.fieldDetails} >
                     {props.children}
                </div>
        </div>
    )
}

export const TimelineFeedContent = (props) => {
    const { headline, image, content, noOfLikes } = props
    return (
        <div style={{padding: '5px', position:'relative'}} >
            <div style={{textAlign: 'center'}}><span className={styles.postHeader}> {headline} </span> </div>
            <div className={styles.postMessage} > {content} </div>
        </div>
    )
}

export const TimelineFeedInteractions = (props) => {
    return (
         <div className={styles.feedActionButtonContainer} >
            <div>
                <button className={styles.feedActionButton} >
                    <div style={{position: 'relative', top: '-3px', textAlign: 'center'}} ><i className={styles.materialIcons} style={{fontSize: '1.9em', marginLeft: '10px',textAlign: 'center'}} >&#xE87E;</i> </div>
                </button>
                <Link to="/" style={{textDecoration: 'none', position: 'relative', top: '0.5em'}}>read more ...</Link>
            </div>
        </div>
    )
}

export const TimelineFeedReports = (props) => {
    const { date } = props
    return (
         <div>
             <Link to="/" className={styles.feedActionButton} style={{float: 'right'}}>
                <span style={{position: 'relative', top: '-3px'}}>23 comments</span>
            </Link>
        </div>
    )
}

