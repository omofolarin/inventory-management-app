import { createStore,  compose } from 'redux';
import { rootReducer as App } from './rootReducer';
import { reactReduxFirebase, firebaseStateReducer } from 'react-redux-firebase';
import { persistStore, persistReducer } from 'redux-persist';
import immutableTransform from 'redux-persist-transform-immutable';
import firebase from 'firebase';
import storage from 'redux-persist/lib/storage'; // defaults to localStorage for web and AsyncStorage for react-native
import hardSet from 'redux-persist/lib/stateReconciler/hardSet'
// const firebaseConfig = {
//     apiKey: process.env.API_KEY,
//     authDomain: process.env.AUTH_DOMAIN,
//     databaseURL: process.env.DATABASE_URL, 
//     projectId: process.env.PROJECT_ID,
//     storageBucket: process.env.STORAGE_BUCKET
// }

const firebaseConfig = {
    projectId: "inventorymanagement-1d3d4",
    apiKey: "AIzaSyBkg1g5TnpVGTWe4fjJYdc3K6QDF5fb5iQ",
    authDomain: "inventorymanagement-1d3d4.firebaseapp.com",
    databaseURL: "https://inventorymanagement-1d3d4.firebaseio.com",
    storageBucket: "inventorymanagement-1d3d4.appspot.com",
};

const persistConfig = {
    transforms: [immutableTransform()],
    key: 'root',
    storage,
    stateReconciler: hardSet,
    blacklist: ['form']
};

const reactReduxFirebaseConfig = {
    userProfile: 'users',
    firebaseStateName: 'firebase',
    profileParamsToPopulate: [
        { child: 'role', root: 'roles' }, // populates user's role with matching role object from roles
    ],
};

firebase.initializeApp(firebaseConfig);

const persistedReducer = persistReducer(persistConfig, App);
const createStoreWithFirebase = compose(
    reactReduxFirebase( firebase, reactReduxFirebaseConfig)
)(createStore);



const store = createStoreWithFirebase(
    persistedReducer, 
    /* preloadedState, */ 
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

let persistor = persistStore(store);

export { store, persistor };
