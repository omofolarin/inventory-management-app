export const TOGGLE_SIDEBAR = 'TOGGLE_SIDEBAR'

export const toggleSidebar = (isToggled) => {
    return { type: TOGGLE_SIDEBAR, isSidebarToggled: isToggled}
}