/**
 * fetching,
 * fetched
 * authSuccess
 * authError
 */
export const FETCHING = 'FETCHING'
export const FETCHED = 'FETCHED'
export const AUTH_SUCCESS =  'AUTH_SUCCESS'
export const AUTH_ERROR = 'AUTH_ERROR'


export const  fetching = () => {
    return { type: FETCHING }
}

export const fetched = () => {
    return { type: FETCHED }
}

export const authSuccess = () => {
    return { type: AUTH_SUCCESS }
}

export const authError = (message) => {
    return { type: AUTH_ERROR, message: message }
}


