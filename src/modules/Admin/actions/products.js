export const ACTIVATE_PRODUCT_MODAL = 'ACTVIATE_PRODUCT_MODAL';
export const DEACTIVATE_PRODUCT_MODAL = 'DEACTIVATE_PRODUCT_MODAL';
export const SELECT_ACTIVE_PRODUCT = 'SELECT_ACTIVE_PRODUCT';
export const DESELECT_ACTIVE_PRODUCT = 'DESELECT_ACTIVE_PRODUCT';


export const activateProductModal = () => {
    return { type: ACTIVATE_PRODUCT_MODAL}
}

export const deactivateProductModal = () => {
    return { type: DEACTIVATE_PRODUCT_MODAL}
}

export const selectActiveProduct = (productId) => {
    return { type: SELECT_ACTIVE_PRODUCT, productId }
}

export const deselectActiveProduct = () => {
    return { type: DESELECT_ACTIVE_PRODUCT }
}