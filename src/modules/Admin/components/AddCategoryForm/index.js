import React, { PureComponent } from 'react';
import { Field, reduxForm } from  'redux-form';
import { BarLoader } from 'react-spinners';
import { TextInput } from 'components/FormInput';
import Button from 'components/Button';
import styles from '../../styles/main.scss';

export class AddCategoryForm extends PureComponent {

    componentWillReceiveProps(nextProps) {
        if ( nextProps.submitSucceeded === true) {
            this.props.onSubmitNewCategorySucceeded();
            this.props.reset();
        } 
    }
   
    render () {
        const { handleSubmit, isSubmitting, submitSucceeded, onSubmitNewCategorySucceeded } = this.props;
       
        return (
            <div className={styles.card}>
                <div style={{marginLeft: 'auto', marginRight: 'auto', width: '15em', paddingBottom: '1.5em'}}>
                    <BarLoader width={250} height={3} loading={isSubmitting} color={'#3d70ad'}/>
                </div>
    
                <h1 style={{fontSize: '22px', fontFamily: 'Roboto Thin', textAlign: 'center', letterSpacing: '1px', color: 'black', lineHeight: '2px'}} >New Category</h1>
                <form onSubmit={handleSubmit}>
                    <Field 
                        component={TextInput}
                        name="title"
                        label="Title"
                        type="text"
                    />
                    <Field 
                        component={TextInput}
                        name="description"
                        label="Description"
                        type="text"
                    />
                    <Button title="Add" type="flat" className={styles.alignAddButton} />
                </form>
            </div>
        );
    }
}
export default reduxForm({
    form: 'addCategory'
})(AddCategoryForm);

