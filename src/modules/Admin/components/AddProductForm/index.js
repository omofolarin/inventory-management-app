import React, { PureComponent } from 'react';
import { Field, reduxForm } from  'redux-form';
import { BarLoader } from 'react-spinners';
import { TextInput, VirtualizedSearchableSelect } from 'components/FormInput';
import Button from 'components/Button';
import styles from '../../styles/main.scss';
import componentStyle from './style.scss';


const normalizeOptions = (categories) => {
    let options = []
    Object.values(categories).map((category, i) => {
        let item = {};
        item.label = category.title;
        options.push(item);
    });
    Object.keys(categories).map((categoryId, i) => {
        options.map((option, j) => {
            if ( i === j ) {
                option.value = categoryId;
            }
        })
    });

    return options;
}


class AddProductForm extends PureComponent {
    constructor(props) {
        super(props);
    }

    render() {
        const { handleSubmit, categories }  = this.props;
        // console.log(Object.keys(categories));
      
        let list = categories != null ? categories : [];
        let options = normalizeOptions(list);
        
        return (
            <div>
                <form onSubmit={handleSubmit}>
                    <Field 
                        component={TextInput}
                        label="Title"
                        name="title"
                    />
                    <Field 
                        component={TextInput}
                        label="Description"
                        name="description"
                    />
                    <Field 
                        component={VirtualizedSearchableSelect}
                        label="Category"
                        name="category"
                        options={options}
                    />
                    <Field 
                        component={TextInput}
                        label="No Of Items"
                        name="noOfItems"
                    />
                    <Field 
                        component={TextInput}
                        label="Selling Price"
                        name="sellingPrice"    
                    />
                    <Button title="Add" type="flat" className={styles.alignAddButton} />
                </form>
            </div>
        );
    }
}

export default reduxForm({
    form: 'addProduct'
})(AddProductForm)