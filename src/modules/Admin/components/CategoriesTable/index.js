import React from 'react';
import { Table, TableHeader, TableRow, TableColumn, TablePagination } from 'components/Table';
import { MdFeedback } from 'react-icons/lib/md/';

export const CategoriesTable = (props) => {
    const { categories, isLoaded, isEmpty } = props;
    let display = null;

    
    
    if ( isEmpty(categories) ) {
        display = <h5>No data found</h5>
    } else {
        
        let categoriesRow =  categories.map((category, i) => {
            const { title, description, slug, noOfProducts } = category;
            return <    TableRow key={i}>
                        <TableColumn content={i + 1} />
                        <TableColumn content={title} />
                        <TableColumn content={description} />
                        <TableColumn content={noOfProducts} />
                    </TableRow>
        });
        display =  <Table style={{boxShadow: 'none'}}>
                        <TableHeader>
                            <TableColumn content={'S/N'} />
                            <TableColumn content={'Title'} />
                            <TableColumn content={'Description'} />
                            <TableColumn content={'No of Products'} />
                        </TableHeader>
                        { categoriesRow }
                        <TablePagination />
                    </Table>;
    }


    return(
        <div>
            <h1 style={{fontSize: '22px', fontFamily: 'Roboto Thin', textAlign: 'center', letterSpacing: '1px', color: 'black', lineHeight: '2px'}}>Categories</h1>
            { display } 
        </div>
    )
}

export default CategoriesTable