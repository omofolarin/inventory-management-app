import React from 'react';
import PropTypes from 'prop-types';
import {
    Header,
    HeaderTitle,
} from 'components/Header';
import Avatar from 'components/Avatar';
import styles from './../styles/main.scss';

export const AdminHeader = () => (
    <Header> 
        <HeaderTitle title="Inventory Management"/>
        <div className={styles.headerAvartar}>
            <Avatar shape="circle" size='2.2' style={{float: 'right'}} /> 
        </div>
    </Header>
    );

export default AdminHeader;
