import React from 'react'
import PropTypes from 'prop-types'
import styles from '../styles/main.scss'
import {
    Field,
    reduxForm
} from 'redux-form'
import {
    EmailInput,
    PasswordInput,
} from 'components/FormInput'
import {
    DualRingSpinner
} from 'components/Spinner'
import {
    Link
} from 'react-router-dom'
import Button from 'components/Button'

export const LoginForm = (props) => {
    const { 
        handleSubmit, 
        fetching,
        fetched,
        submitFailed,
        authErrorMessage,
    } = props

    let access = fetching && !submitFailed ? <div style={{float: 'right', marginRight: '5%', marginTop: '-1%'}}><DualRingSpinner/></div> : <Button type="flat" title="Access" className={styles.alignAddButton} />
    let errorMsg = authErrorMessage != null ? <div style={{backgroundColor: 'pink',padding: '0.5em', margin:'2%', marginLeft: '8%',}}><span>{authErrorMessage}</span></div> : null
    return(
        <div className={styles.loginPanel}>
            <div className={styles.loginPanelHeading}>
                    <h1 className={styles.loginPanelTitle} style={{textAlign: 'center', fontSize: '1.6em', fontFamily: 'Roboto Thin'}}>Login</h1>
            </div>
            <form onSubmit={handleSubmit}>
                {errorMsg}
                <Field 
                    name="email"
                    component={EmailInput}
                    type="email"
                    label="Email Address"
                />
                <Field 
                    name="password"
                    component={PasswordInput}
                    type="password"
                    label="Password"
                />
                {access}
                <p className={styles.alignWithInput}><Link to="/" style={{fontSize: '14px', textDecoration: 'none', marginLeft: '2.5em', position: 'relative', top: '1.5em'}}>Forgot Password?</Link></p>
            </form>
          
        </div>       
    )
}

export default reduxForm({
    form: 'adminLogin',
})(LoginForm)