import React from 'react'
import { Table, TableHeader, TableRow, TableColumn, TablePagination } from 'components/Table'


export const ManagersTable = (props) => {
    const { managers } = props;
    let managersRow =  managers.map((manager, i) => {
        const { fullName, email, active } = manager;
        return <TableRow key={i}>
                    <TableColumn content={i + 1} />
                    <TableColumn content={fullName} />
                    <TableColumn content={email} />
                    <TableColumn content={active} />
                </TableRow>
    })
    return(
        <div>
            <h1 style={{fontSize: '22px', fontFamily: 'Roboto Thin', textAlign: 'center', letterSpacing: '1px', color: 'black', lineHeight: '2px'}}>Managers</h1>
            <Table style={{boxShadow: 'none'}}>
                <TableHeader>
                    <TableColumn content={'S/N'} />
                    <TableColumn content={'Full Name'} />
                    <TableColumn content={'Email'} />
                    <TableColumn content={'Active'} />
                
                </TableHeader>
                { managersRow }
                <TablePagination />
            </Table>
        </div>
    )
}

export default ManagersTable