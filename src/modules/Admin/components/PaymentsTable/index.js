import React from 'react'
import { Table, TableHeader, TableRow, TableColumn, TablePagination } from 'components/Table'


export const PaymentsTable = (props) => {
    const { payments } = props;
    let paymentsRow =  payments.map((payment, i) => {
        const { fullName, products, quantity, cost, date } =  payment;
        return <TableRow key={i}>
                    <TableColumn content={i + 1} />
                    <TableColumn content={fullName} />
                    <TableColumn content={products} />
                    <TableColumn content={quantity} />
                    <TableColumn content={cost} />
                    <TableColumn content={date} />
                </TableRow>
    })
    return(
        <div>
            <h1 style={{fontSize: '22px', fontFamily: 'Roboto Thin', textAlign: 'center', letterSpacing: '1px', color: 'black', lineHeight: '2px'}}>Payments</h1>
            <Table style={{boxShadow: 'none'}}>
                <TableHeader>
                    <TableColumn content={'S/N'} />
                    <TableColumn content={'Full Name'} />
                    <TableColumn content={'Products'} />
                    <TableColumn content={'Quantity'} />
                    <TableColumn content={'Tolal Costs'} />
                    <TableColumn content={'Date'} />
                    
                </TableHeader>
                { paymentsRow }
                <TablePagination />
            </Table>
        </div>
    )
}

export default PaymentsTable