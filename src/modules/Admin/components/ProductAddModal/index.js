import React, { PureComponent } from 'react';
import { Modal, ModalContent } from 'components/Modal';
import { FileInput } from 'components/FormInput';
import styles from '../../styles/main.scss';
import { MdImage, MdCancel } from 'react-icons/lib/md';
import { Field, reduxForm } from 'redux-form';
import Button from 'components/Button';
import QRCode from 'qrcode-react';               

export const ProductAddModal = (props) => {
    const { isActive, handleSubmit, onChangeImage, uploadSuccess, productId, cancelModal } = props;
    let display = null;
    
    if (!uploadSuccess ) {
        display =   <form onSubmit={handleSubmit}>
                        <h1 style={{fontFamily: 'Roboto Thin', fontWeight: 'bold', fontSize: '1.7em', textAlign: 'center', letterSpacing: '1.5px', paddingTop: '1em'}}> Upload Images</h1>
                        <Field
                            name="productImage"
                            component={FileInput} 
                            children={ 
                                <div style={{padding: '1em'}}>
                                    <div style={{ marginLeft: '1.5em' , marginRight: 'auto', width: 'auto', display: 'block', padding: '0.5em'}}>
                                        <MdImage size={100} color="#cccccc"/>
                                    </div>
                                    <p style={{ fontFamily: 'Roboto Regular', textAlign: 'center', fontSize: '14px'}}>Try dropping some files here, or click to select files to upload.</p>
                                </div>
                            }
                        />
       
                        <Button title="Upload" type="flat" className={styles.alignAddButton} />             
                    </form>;
    } else {
       display =    <div>
                        <h1 style={{fontFamily: 'Roboto Thin', fontWeight: 'bold', fontSize: '1.7em', textAlign: 'center', letterSpacing: '1.5px', paddingTop: '1em'}}> Upload Images</h1>
                        <QRCode value={productId} />
                    </div> 
    }
    return (
       <div>
           <Modal isActive={isActive}>
                    <ModalContent size="large" style={{height: '80vh'}}>
                        <div style={{float: 'right', cursor: 'pointer'}} onClick={cancelModal}>
                            <MdCancel size={26} color="#000000" />
                        </div>
                        <div style={{ marginLeft: 'auto', marginRight: 'auto', width: '40%'}}>
                         { display}
                           
                        </div>
                    </ModalContent>
                </Modal>
       </div>
    )
};

export default reduxForm({
    form: 'productImageUpload',
})(ProductAddModal);