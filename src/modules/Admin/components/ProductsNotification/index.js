import React from 'react'
import { Table, TableHeader, TableRow, TableColumn, TablePagination } from 'components/Table'


export const ProductsNotification = (props) => {
    const { outOfStock, limitedStock } = props;
    let limitedStockRow =  limitedStock.map((limitedStock, i) => {
        const { title, description, category, leftOnShelve } = limitedStock;
        return <TableRow key={i}>
                    <TableColumn content={i + 1} />
                    <TableColumn content={title} />
                    <TableColumn content={category} />
                    <TableColumn content={description} />
                    <TableColumn content={leftOnShelve} />
                </TableRow>
    })

    let outOfStockRow = outOfStock.map((outOfStock, i) => {
        const { title, category, description} = outOfStock;
        return <TableRow key={i}>
                    <TableColumn content={i + 1} />
                    <TableColumn content={title} />
                    <TableColumn content={category} />
                    <TableColumn content={description} />
               </TableRow>
    })
    return(
        <div>
            <h1 style={{fontSize: '22px', fontFamily: 'Roboto Thin', textAlign: 'center', letterSpacing: '1px', color: 'black', lineHeight: '2px'}}>Limited Stock</h1>
            <Table style={{boxShadow: 'none'}}>
                <TableHeader>
                    <TableColumn content={'S/N'} />
                    <TableColumn content={'Title'} />
                    <TableColumn content={'Category'} />
                    <TableColumn content={'Description'} />
                    <TableColumn content={'Left on shelve'} />
                </TableHeader>
                { limitedStockRow }
                <TablePagination />
            </Table>

            <h1 style={{fontSize: '22px', fontFamily: 'Roboto Thin', textAlign: 'center', letterSpacing: '1px', color: 'black', lineHeight: '2px'}}>Out of Stock</h1>
            <Table style={{boxShadow: 'none'}}>
                <TableHeader>
                    <TableColumn content={'S/N'} />
                    <TableColumn content={'Title'} />
                    <TableColumn content={'Category'} />
                </TableHeader>
                { outOfStockRow }
                <TablePagination />
            </Table>
        </div>
    )
}

export default ProductsNotification