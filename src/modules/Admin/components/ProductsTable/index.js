import React from 'react'
import { Table, TableHeader, TableRow, TableColumn, TablePagination } from 'components/Table'
import { MdFeedBack } from 'react-icons/lib/md/';
// import { isEmpty } from 'react-redux-firebase';


export const ProductsTable = (props) => {
    const { products, isEmpty, isLoaded } = props;
    // console.log(products);
    let display = null;
    
    if ( isEmpty(products) ) {
        display = <h5>No data found</h5>
    } else {
        let productsRow =  products.map((product, i) => {
            // console.log(product);
            const { title, category, description, slug, noOfItems, sellingPrice } =  product.value; 
            return <TableRow key={i}>
                        <TableColumn content={i + 1} />
                        <TableColumn content={title} />
                        <TableColumn content={description} />
                        <TableColumn content={noOfItems} />
                    </TableRow>
        });
        display =  <Table style={{boxShadow: 'none'}}>
                        <TableHeader>
                            <TableColumn content={'S/N'} />
                            <TableColumn content={'Title'} />
                            <TableColumn content={'Description'} />
                            <TableColumn content={'No of Products'} />
                        </TableHeader>
                        {productsRow}
                        <TablePagination />
                    </Table>;
    }


    return(
        <div>
            <h1 style={{fontSize: '22px', fontFamily: 'Roboto Thin', textAlign: 'center', letterSpacing: '1px', color: 'black', lineHeight: '2px'}}>Products</h1>
           {display}
        </div>
    )
}

export default ProductsTable