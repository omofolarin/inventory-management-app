import React from 'react'
import PropTypes from 'prop-types'
import { 
    Sidebar,
    SidebarTitle,
    SidebarHeader,
    SidebarBody,
    SidebarNav,
    SidebarNavToggle,
} from 'components/SidebarSecond'

const AdminSidebar = (props) => {
    const { sidebarNavList } = props
    
    return(
         <Sidebar>
            <SidebarHeader>
                <SidebarTitle title="" url="/" />
                <SidebarNavToggle />
            </SidebarHeader>

            <SidebarBody>
               <SidebarNav navLists={sidebarNavList}/>
            </SidebarBody>
        </Sidebar>
    )
}

AdminSidebar.defaultProps = {
    sidebarNavList: PropTypes.array
}

export default AdminSidebar