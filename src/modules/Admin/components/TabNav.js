import React from 'react'
import PropTypes from 'prop-types'
import { TabNav, TabNavList, TabSearch,TabContent } from 'components/TabNav'

export const AdminTabNav = (props) => {
    const { children, tabList } = props;
    const tabNavList = typeof tabList != 'undefined' ? <TabNavList tabList={tabList} /> : null;
    const tabNavStyle = typeof tabList != 'undefined' ? {width: 'auto'} : {height: '2em'};
    const tabSearchStyle = typeof tabList != 'undefined' ? { position: 'relative'} : {zIndex: 25, position: 'relative', top: '2.1em'};
    return(
        <div>

            <TabNav style={tabNavStyle}>
                { tabNavList }
                <TabSearch style={tabSearchStyle}/>
            </TabNav>
            <TabContent>
                {children}
            </TabContent>
        </div>
    )
}

export default AdminTabNav
