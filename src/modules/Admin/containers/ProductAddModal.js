import { connect } from 'react-redux';
import ProductAddModal from '../components/ProductAddModal/';
import {
    selectActiveCategory
} from '../actions/products';

const mapStateToProps = (state, ownProps) => {
    let values = {};
    values.category = state.Admin.Product.toJS().activeCategory;
    console.log(values.category);
    return {
        initialValues: values,
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        setCategory: () => {
            dispatch(actionCreator)
        }
    }
}
export default connect(mapStateToProps)(ProductAddModal);