import React from 'react'
import { connect } from 'react-redux'
import * as actionCreators from './../actions/Header'
import Sidebar  from '../components/Sidebar'

const mapStateToProps = (state) => {
    let rawState = state.toJS();
    return {
        
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        toggleSidebar: (toggled) => {
            dispatch(actionCreators.toggleSidebar)
        }

    }
}

export const SidebarContainer = connect (mapStateToProps, mapDispatchToProps)(Sidebar)