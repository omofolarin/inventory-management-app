import { connect } from 'react-redux'
import adminRoutes, { PrivateRoute }  from '../routes'
import * as actionCreators from '../actions/adminRoutes'
import { authSuccess } from '../actions/auth'

const mapStateToProps = (state, ownProps) => {
    let selector = state.Admin.dashboard.toJS()
    return {
        isSidebarToggled: selector.isSidebarToggled
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        toggleSidebar: (val) => {
            dispatch(actionCreators.toggleSidebar(val))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(adminRoutes)


const privateRouteMapDispatchToProps = (dispatch) => {
    return {
        login : () => dispatch(authSuccess())
    }
}

const privateRouteMapStateToProps = (state) => {
    return {
        authSuccess: state.Admin.auth.toJS().authSuccess
    }
}
export const AuthenticatedRoute = connect(privateRouteMapStateToProps, privateRouteMapDispatchToProps)(PrivateRoute)