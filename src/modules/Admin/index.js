import React, { Component } from 'react'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import  { AdminRoutes as Routes } from './routes.js'
import { Helmet } from 'react-helmet'
import { Auth } from './pages/Auth'


const Add = (props) => (
    <div>
        <h1>Hello david</h1>
    </div>
);

class AdminModule extends Component {
    constructor(props) {
        super (props);
    }

    render () {
        return(
            <div>
                <Helmet>
                    <meta charSet="utf-8" />
                    <title>Inventory management - Dashboard</title>
                </Helmet>
                <Switch>
                    <Route path="/login" component={Auth} />
                    <Route path='/' component={Routes} />
                </Switch>
            </div>
        )
    }
};

export default AdminModule
