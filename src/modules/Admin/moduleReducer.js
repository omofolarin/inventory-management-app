import { combineReducers } from 'redux'
import Product from './reducers/Product'

const AdminModuleReducer = combineReducers({
   Product,
});

export default AdminModuleReducer;