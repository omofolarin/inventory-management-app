import { all } from 'redux-saga/effects'
import {
    watchFetchedClans,
} from './sagas/clans'


export function* moduleSaga() {
    yield all([
        watchFetchedClans(),
        
    ])
}