import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styles from '../../styles/main.scss'
import LoginForm from '../../components/LoginForm'
import {
    withRouter,
    Route,
    Redirect
} from 'react-router-dom'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { firebaseConnect } from 'react-redux-firebase'
import { SyncLoader } from 'react-spinners'


const  AuthPage = withRouter((props) => {
    const { authError, authDetails, firebase, initializingFirebase } = props;

    const storage = JSON.parse(JSON.parse(JSON.parse(localStorage.getItem('persist:root')).firebase));
    console.log(storage);
    if (storage) {
        let expiryTime = storage.auth.stsTokenManager.expirationTime;
        if ( expiryTime > Date()  || (authError == null && authDetails != null) ) { 
            return <Redirect to={'/admin/categories'} />;
        }

    }
    const submit  = (values) => { 
        firebase.login(values);      
    }

   
    const display = initializingFirebase === true || undefined ?  <div style={{marginLeft: 'auto', marginRight: 'auto', width: '5em', marginTop: '45vh'}}>
                                                <SyncLoader
                                                    color={'#123abc'} 
                                                    loading={true} 
                                                    size={9}    
                                                />
                                            </div>
                                        :   <div>
                                                <div className={styles.loginAppLogo}>
                                                    <h1 className={styles.loginAppTitle} style={{fontSize: '25px', fontFamily: 'Roboto Thin', textAlign: 'center', letterSpacing: '1.5px', position: 'relative', top: '3em'}} >Inventory Management System</h1>
                                                </div>
                                                <div className={styles.card} style={{marginLeft: 'auto', marginRight: 'auto', width: '30%', marginTop: '7%', }}> 
                                                    <LoginForm onSubmit={submit} />           
                                                </div>
                                            </div>;
    

        return (
            <div className={styles.pageBody}>
              { display }  
            </div>
            
        );
    
})



const mapStateToProps = (state) => {
    const { Admin, firebase } = state;
    return {
       authError : firebase.authError,
       authDetails: firebase.auth,
       initializingFirebase: firebase.isInitializing,

    }
}

export const Auth = compose(connect(mapStateToProps), firebaseConnect())(AuthPage)

