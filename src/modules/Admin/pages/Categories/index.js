import React, { PureComponent } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { firebaseConnect, populate, isLoaded, isEmpty } from 'react-redux-firebase';
import {
    isValid,
    isSubmitting,
    hasSubmitSucceeded,
    hasSubmitFailed,
    getFormValues,
} from 'redux-form'
import { MdSearch } from 'react-icons/lib/md';
import { ToastContainer, toast } from 'react-toastify';
import { ClipLoader } from 'react-spinners';
import styles from '../../styles/main.scss'
import CategoriesTable from '../../components/CategoriesTable/';
import AddCategoryForm from '../../components/AddCategoryForm/';


export class Categories extends PureComponent {
    constructor(props) {
        super(props)
    }
   
    
    render() {
        const { categories, submitNewCategory, submittingCategory, submitCategorySucceeded, submitCategoryFailed, onSubmitNewCategorySucceeded } = this.props;
        let categoryList = typeof categories === 'object' && !isEmpty(categories) ? Object.values(categories) : null;

        let display = null;
        if ( !isLoaded(categories)) {
            display = <div style={{width: '10%', marginLeft: 'auto', marginRight: 'auto', marginTop: '15%'}}> <ClipLoader size={50} color={'#3d70ad'}/></div>;
        } else {
            display = <CategoriesTable categories={categoryList} isLoaded={isLoaded} isEmpty={isEmpty} />; 
        }


        return (
            <div className={styles.pageLayout}>
                <div className={styles.left}>
                    <div className={styles.card}>
                        { display }
                    </div>
                </div>
                <div className={styles.right}>
                    <AddCategoryForm onSubmit={submitNewCategory} onSubmitNewCategorySucceeded={onSubmitNewCategorySucceeded} isSubmitting={submittingCategory} submitSucceeded={submitCategorySucceeded}/>;
                </div>
                <ToastContainer/>
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    const { firebase } = state;
    
    return {
        firebase: firebase,
        submittingCategory: isSubmitting('addCategory')(state),
        submitCategorySucceeded: hasSubmitSucceeded('addCategory')(state),
        submitCategoryFailed: hasSubmitFailed('addCategory')(state),
        categories: firebase.data.categories
    }
};


const mapDispatchToProps = (dispatch, ownProps) => {
    const { firebase } = ownProps;
    

    return {
        submitNewCategory : (values) => {
            return firebase.push('categories/', values).then((snapshot) => {
                console.log(snapshot)
            });
        
        },
        onSubmitNewCategorySucceeded: () => {
            return toast(" New Category has been added");
        }
    }
}

export default compose(
    firebaseConnect((props)=> [
        { path: 'categories'}
    ]),
    connect( mapStateToProps, mapDispatchToProps),
)(Categories)
