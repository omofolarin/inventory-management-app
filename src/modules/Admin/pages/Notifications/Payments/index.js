import React from 'react'
import { NavLink } from 'react-router-dom'
import PaymentsTable from '../../../components/PaymentsTable/'
import styles from '../../../styles/main.scss'
import { TextInput } from 'components/FormInput'
import Button from 'components/Button'
import { MdSearch } from 'react-icons/lib/md'
const payments = [
    {   
       id: 1,
       fullName: 'shonibare omofolarin',
       products: 'tooth pastes, relaxer, hand wash, haircream',
       quantity: 15,
       cost: 'N5000',
       date: '15/01/2018'
    },
    {
        id: 2,
        fullName: 'shonibare omofolarin',
        products: 'tooth pastes, relaxer, hand wash, haircream',
        quantity: 15,
        cost: 'N5000',
        date: '15/01/2018'
    },
    {
        id: 3,
        fullName: 'shonibare omofolarin',
        products: 'tooth pastes, relaxer, hand wash, haircream',
        quantity: 15,
        cost: 'N5000',
        date: '15/01/2018'
    },
    {
        id: 4,
        fullName: 'shonibare omofolarin',
        products: 'tooth pastes, relaxer, hand wash, haircream',
        quantity: 15,
        cost: 'N5000',
        date: '15/01/2018'
    },
    
];


export const Payments = (props) => {
    return(
        <div className={styles.pageLayout}>
            <div>
                <div className={styles.card} style={{marginTop: '1.8em', width: '100%', marginRight: 'auto', marginLeft: '10%' }}>
                    <PaymentsTable payments={payments}/>
                </div>
                <Button 
                    type="fab" 
                    icon={<MdSearch color="#ffffff" size={20} />}
                    style={{position: 'relative', top: '31vh'}}
                />
            </div>
        </div>
    )
}

export default Payments