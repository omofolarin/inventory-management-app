import React from 'react'
import ProductsNotification from '../../../components/ProductsNotification'
import styles from '../../../styles/main.scss'


const outOfStock = [
    {
        id: 1,
        title: 'indomie',
        category: 'noodles'
    },
    {
        id: 2,
        title: 'sun rise detergent',
        category: 'laundry'
    },
    {
        id: 3,
        title: 'Aim small Miss small',
        category: 'books'
    }
];

const limitedStock = [
    {
        id: 1,
        title: 'dudo osun bathing soup',
        category: 'skin care',
        leftOnShelve: 15,
    },
    {
        id: 2,
        title: 'detol bathing soup',
        category: 'skin care',
        leftOnShelve: 20,
    }
];

export const Products = (props) => {
    return(
        <div>
            <div className={styles.card} style={{marginLeft: '5%', marginRight: '5%', }}>
                <ProductsNotification   outOfStock={outOfStock} limitedStock ={limitedStock}/>
            </div>
            
        </div>
    )
}

export default Products