import React from 'react'
import { Route, route, NavLink, Switch, Redirect } from 'react-router-dom'
import Payments from './Payments/'
import Products from './Products/'
import styles from '../../styles/main.scss'


export const Notifications = (props) => {

    return (
        <div className={styles.pageLayout}>
            <div className={styles.pageTab} style={{display: 'inline-block', width: '100%', backgroundColor: 'white'}}>
                <div>
                    <NavLink to="/admin/notifications/payments" activeStyle={{background: '#f56d21', color: 'white'}} style={{textDecoration: 'none', float: 'left', marginBottom: '5px', padding: '0.5em', background: '#f2f2f2', marginLeft: '1em', borderRadius: '5px', fontWeight: 'bold', fontSize: '15px', marginTop: '5px', color: 'black', letterSpacing: '1px' }}>Payment</NavLink>
                </div>
                <div>
                    <NavLink to="/admin/notifications/products" activeStyle={{background: '#f56d21', color: 'white'}} style={{textDecoration: 'none', float: 'left', marginBottom: '5px', padding: '0.5em', background: '#f2f2f2', marginLeft: '1em', borderRadius: '5px', fontWeight: 'bold', fontSize: '15px', marginTop: '5px', color: 'black', letterSpacing: '1px'}}>Products</NavLink>
                </div>
            </div>
            <Switch>
                <Route  path="/admin/notifications/payments" component={Payments} />
                <Route  exact path="/admin/notifications/products" component={Products} />
                <Redirect from='/admin/notifications' to='/admin/notifications/payments' />
            </Switch>

        </div>
    )
}

export default Notifications