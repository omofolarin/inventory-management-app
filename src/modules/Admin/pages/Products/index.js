import React from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { firebaseConnect, populate, isLoaded, isEmpty } from 'react-redux-firebase'
import {
    getFormValues,
    isValid,
    isSubmitting,
    hasSubmitSucceeded,
    hasSubmitFailed,
} from 'redux-form';
import { ClipLoader } from 'react-spinners';
import { ToastContainer, toast } from 'react-toastify';
import Button from 'components/Button';
import ProductsTable from '../../components/ProductsTable/';
import AddProductForm from '../../components/AddProductForm/';
import ProductAddModal from '../../components/ProductAddModal/';
import {
    activateProductModal,
    deactivateProductModal,
    selectActiveProduct,
    deselectActiveProduct,
} from '../../actions/products'
import styles from '../../styles/main.scss';

const dataURItoBlob = (dataURI) => {
    // convert base64 to raw binary data held in a string
    // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
    var byteString = atob(dataURI.split(',')[1]);

    // separate out the mime component
    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]

    // write the bytes of the string to an ArrayBuffer
    var ab = new ArrayBuffer(byteString.length);
    var ia = new Uint8Array(ab);
    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }

    // write the ArrayBuffer to a blob, and you're done
    var bb = new Blob([ab]);
    return bb;

}



export const Products = (props) => {
    const  { 
        auth, 
        addNewProduct, 
        submittingProduct, 
        submitProductFailure, 
        submitProductImageSuccess, 
        products, 
        categories, 
        onUploadImage,
        productId,
        deactivateModal,
        isModalActive
    } = props;
    let displayForm = null;
    let display = null;

    if ( !isLoaded(categories)) {
       displayForm = <div style={{width: '10%', marginLeft: 'auto', marginRight: 'auto', marginTop: '15%'}}> <ClipLoader size={50} color={'#3d70ad'}/></div>; 
    } else {
        displayForm = <AddProductForm onSubmit={addNewProduct} categories={categories} />;
    }

    if ( !isLoaded(products)) {
        display = <div style={{width: '10%', marginLeft: 'auto', marginRight: 'auto', marginTop: '15%'}}> <ClipLoader size={50} color={'#3d70ad'}/></div>;
    } else {
        let productsList = typeof products === 'object' ? products : [];
        display = <ProductsTable products={productsList} isEmpty={isEmpty}/>;
    }
    

    return(
        <div className={styles.pageLayout}>
           <ProductAddModal 
                onSubmit={onUploadImage} 
                isActive={isModalActive}  
                cancelModal={deactivateModal}
                uploadSuccess={submitProductImageSuccess}
                productId={productId}
            />
            <div className={styles.left}>
                <div className={styles.card}>
                    {display}
                </div>
            </div>
            <div className={styles.right}>
                <div className={styles.card} style={{height: '33em'}}>
                    <h1 style={{fontSize: '22px', fontFamily: 'Roboto Thin', textAlign: 'center', letterSpacing: '1px', color: 'black', lineHeight: '2px'}} >New Product</h1>
                    {displayForm}
                </div>
            </div>
        </div>
    )
}


const mapStateToProps = (state, ownProps) => {
    const { firebase, Admin } = state;
   
    return {
        productId: Admin.Product.toJS().activeProduct,
        submittingProduct: isSubmitting('addProduct')(state),
        submitProductImageSuccess: hasSubmitSucceeded('productImageUpload')(state),
        categories: firebase.data.categories,
        products: firebase.ordered.products,
        auth: firebase.auth,
        isModalActive: Admin.Product.toJS().isModalActive
    }
};



const mapDispatchToProps = (dispatch, ownProps) => {
    const { firebase } = ownProps;

    return ({
        addNewProduct : (values) => {
            let activeProduct = null;
            firebase.push('products/', values)
            .then((snapshot) => {
                activeProduct = snapshot.key;
                dispatch(selectActiveProduct(activeProduct));
                // return activeProduct;
            });
            dispatch(activateProductModal());
        },
        // onUploadImage,
        onAddNewProductSuccess: () => {
            return toast(" New Product has been added");
        },
        
        deactivateModal: () => {
            dispatch(deactivateProductModal());
        },
    });  
    
};



const mergeProps = (stateProps, dispatchProps, ownProps) => {
    return ({
        ...ownProps,
        ...dispatchProps,
        ...stateProps,
        onUploadImage : (imagefile) => {
            const { firebase } = ownProps;
            let product = stateProps.productId;
            let data = imagefile.productImage;
            // console.log(data)
            data.map((file) => {
                let reader = new FileReader();
                reader.onload = (e) => {
                    let blob = e.target.result;
                    let blobData = dataURItoBlob(blob);
                    firebase.uploadFile('products', blobData, '/products/' + product + '/images/' , { name: file.name }).then((snapshots)=> console.log('image snapshot', snapshots));
                };
                if (file) {
                    reader.readAsDataURL(file);
                } 
            })
        }
        
    });
}
export default compose(
    firebaseConnect((props) => [ { path: 'products'}]),
    connect(
        mapStateToProps, 
        mapDispatchToProps,
        mergeProps
    )
)(Products);