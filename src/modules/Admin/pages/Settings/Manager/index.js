import React from 'react';
import styles from '../../../styles/main.scss';
import { Switch, Route, Link, Redirect } from 'react-router-dom';
import { TextInput } from 'components/FormInput';
import Button from 'components/Button';
import ManagersTable from '../../../components/ManagersTable/';


const managers = [
    {
        fullName: 'shonibare omofolarin',
        email: 'folarinshonibare@gmail.com',
        active: true,
    },

    {
        fullName: 'david wisdom',
        email: 'davidwisdom@gmail.com',
        active: false,
    },
    {
        fullName: 'ayodeji shomolu',
        email: 'aydeji@gmail.com',
        active: false
    }
];


export const Manager = (props) => {
    return (
        <div>
            <div>
                <div className={styles.left}>
                    <div className={styles.card}>
                        <ManagersTable managers={managers} />
                    </div>
                </div>

                <div className={styles.right}>
                    <div className={styles.card} >
                        <Switch>
                            <Route path="/admin/settings/manager/reset-password" component={ResetPassword} />
                            {/* <Redirect from='/admin/settings' to='/admin/settings/manager/reset-password' /> */}
                        </Switch>
                    </div>
                </div>

            </div>
        </div>
    )
}

export const ResetPassword = (props) => {
    return (
        <div>
            <h1 style={{fontSize: '22px', fontFamily: 'Roboto Thin', textAlign: 'center', letterSpacing: '1px', color: 'black', lineHeight: '2px'}}>Reset Password</h1>
            <TextInput input={{name: 'password'}} meta={{active: false}} label="New Password"/>
            <TextInput input={{name: 'cPassword'}} meta={{active: false}} label="Confirm New Password"/>
            <Button title="Reset" type="flat" className={styles.alignAddButton}/>
            <Link to="/admin/settings/manager/add-new-manager">Add New Manager </Link>
        </div>
    )
}



export default Manager