import React from 'react'
import styles from '../../../styles/main.scss'

export const Notifications = (props) => {
    return (
        <div>
            <div className={styles.card} style={{width: '30%', marginLeft: '2%', padding: '1%'}}>
                <h1 style={{fontSize: '22px', fontFamily: 'Roboto Thin', textAlign: 'center', letterSpacing: '1px', color: 'black', lineHeight: '2px'}}>Manager Notification Settings</h1>
                <p>Default phone: 09083494644</p>
                <p>Send notification to all Managers: YES</p>
                <p>Notify On Out of Stock: YES</p>
                <p>Notify On Limited Stock: YES</p>
                <p>Notify On payment: YES</p>
            </div>
        </div>
    )
}

export default Notifications