import React from 'react'
import { Route, route, NavLink, Switch, Redirect, Link } from 'react-router-dom'
import Manager from './Manager/'
import Notifications from './Notifications/'
import styles from '../../styles/main.scss'
import { TextInput } from 'components/FormInput';
import Button from 'components/Button';


export const AddNewManager = (props) => {
    return (
        <div className={styles.card} style={{width: '40%', marginLeft: '25%'}}>
            <h1 style={{fontSize: '22px', fontFamily: 'Roboto Thin', textAlign: 'center', letterSpacing: '1px', color: 'black', lineHeight: '2px'}}>Add New Manager</h1>
            <TextInput input={{name: 'fullName'}} meta={{active: false}} label="Full Name"/>
            <TextInput input={{name: 'emailAddress'}} meta={{active: false}} label="Email Address"/>
            <TextInput input={{name: 'password'}} meta={{active: false}} label="Password" />
            <TextInput input={{name: 'cPassword'}} meta={{active: false}} label="Confirm Password" />
            <TextInput input={{name: 'phoneNumber'}} meta={{active: false}} label="phoneNumber" />            
            <Button title="Add" type="flat" className={styles.alignAddButton} label="Password" />
            {/* <Link to="/admin/settings/manager/reset-password">Reset Password</Link> */}
        </div>
    )
}



export const Settings = (props) => {

    return (
        <div className={styles.pageLayout}>
            <div className={styles.pageTab} style={{display: 'inline-block', width: '100%', backgroundColor: 'white'}}>
                <div>
                    <NavLink to="/admin/settings/manager/reset-password" activeStyle={{background: '#f56d21', color: 'white'}} style={{textDecoration: 'none', float: 'left', marginBottom: '5px', padding: '0.5em', background: '#f2f2f2', marginLeft: '1em', borderRadius: '5px', fontWeight: 'bold', fontSize: '15px', marginTop: '5px', color: 'black', letterSpacing: '1px' }}>Manager</NavLink>
                </div>
                <div>
                    <NavLink to="/admin/settings/notifications" activeStyle={{background: '#f56d21', color: 'white'}} style={{textDecoration: 'none', float: 'left', marginBottom: '5px', padding: '0.5em', background: '#f2f2f2', marginLeft: '1em', borderRadius: '5px', fontWeight: 'bold', fontSize: '15px', marginTop: '5px', color: 'black', letterSpacing: '1px'}}>Notifications</NavLink>
                </div>
            </div>
            <Switch>
                <Route exact path="/admin/settings/manager/reset-password" component={Manager} />
                <Route  exact path="/admin/settings/notifications" component={Notifications} />
                <Route exact path="/admin/settings/manager/add-new-manager" component={AddNewManager} />
                <Redirect from='/admin/settings' to='/admin/settings/manager/reset-password' />

            </Switch>

        </div>
    )
}

export default Settings