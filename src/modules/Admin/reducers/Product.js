import {
    ACTIVATE_PRODUCT_MODAL,
    DEACTIVATE_PRODUCT_MODAL,
    SELECT_ACTIVE_CATEGORY,
    DESELECT_ACTIVE_CATEGORY,
    DESELECT_ACTIVE_PRODUCT,
    SELECT_ACTIVE_PRODUCT,
} from '../actions/products';
import { Map } from 'immutable';

const initialState = Map({
    isModalActive: false,
    activeProduct: null,
});

export const Products = (state = initialState, action) => {
  
    switch (action.type) {
        case ACTIVATE_PRODUCT_MODAL:
            return state.set('isModalActive', true);

        case DEACTIVATE_PRODUCT_MODAL:
            return state.set('isModalActive', false)
                        .set('isModalActive', false)
                        .set('activeProduct', null );

        case SELECT_ACTIVE_PRODUCT: 
            return state.set('activeProduct', action.productId);

        case DESELECT_ACTIVE_PRODUCT:
            return state.set('activeProduct', null);

        default:
            return state;
    
    }
    
};

export default Products;