import { Map } from 'immutable'
import {
    TOGGLE_SIDEBAR
} from '../actions/adminRoutes'

const initialState = Map({
    isSidebarToggled: false,
})

const dashboard = (state = initialState, action) => {
    switch (action.type) {
        case TOGGLE_SIDEBAR:
           return state.set('isSidebarToggled', !action.isSidebarToggled )
        default:
            return state;
    }
}

export default dashboard