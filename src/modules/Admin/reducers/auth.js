import {
    FETCHING,
    FETCHED,
    AUTH_SUCCESS,
    AUTH_ERROR,
    AUTH_ERROR_MESSAGE
} from '../actions/auth'
import { Map } from 'immutable'

const initialState = Map({
    fetching: null,
    fetched: null,
    authSuccess: null,
    authError: null,
    authErrorMessage: null
})

const auth = (state = initialState, action ) => {
    switch (action.type) {
        case FETCHING:
            return  state.set('authErrorMessage', null)
                         .set('fetching', true) 
        case FETCHED:
            return state.set('fetched', true)
                        .set('fetching', false)
        case AUTH_SUCCESS:
            return state.set('authSuccess', true)
                        .set('authError', null)
                        .set('authErrorMessage', null)
        case  AUTH_ERROR:
             return state.set('authError', true)
                         .set('authSuccess', null)
                         .set('authErrorMessage', action.message)
        default:
            return state;
    }
}

export default auth