import React, { Component } from 'react'
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Link,
  Redirect
} from 'react-router-dom'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { WithFirebase, firebaseConnect } from 'react-redux-firebase'
import { MdSearch } from 'react-icons/lib/md';
import Button from 'components/Button'
import AdminHeader from './components/Header'
import AdminSidebar from './components/Sidebar'
import { MdNotifications, MdAssignment, MdSettings, MdBook, MdCancel } from 'react-icons/lib/md/'
import Categories from './pages/Categories/';
import Products from './pages/Products/';
import Notifications from './pages/Notifications/';
import Payments from './pages/Notifications/Payments/';
import { Products as ProductsNotification } from './pages/Notifications/Products/';
import Settings from './pages/Settings/'
import styles from './styles/main.scss'
import { SearchInput, TextInput } from 'components/FormInput'
import { Auth } from './pages/Auth'

const Add = () => (
    <h1>add</h1>
)
const List = () => (
    <h1>list</h1>
)

 const routes = [
            { 
                id: 1,
                icon: <MdBook size={23} style={{position: 'relative', top: '-0.5em'}} />,
                title: 'Categories',
                path: '/admin/categories',
                component: Categories,
                exact: false,
            },
            { 
                id: 2,
                icon: <MdAssignment size={23} style={{position: 'relative', top: '-0.5em'}}/>    ,
                title: 'Products',
                path: '/admin/products',
                component: Products,
                exact: false,
            },
            { 
                id: 3,
                icon: <MdNotifications size={23} style={{position: 'relative', top: '-0.5em'}} />,
                title: 'Notifications',
                path: '/admin/notifications',
                component: Notifications,
                // exact: false,
            },
            {   
                id: 4,
                icon: <MdSettings size={23} style={{position: 'relative', top: '-0.5em'}} />,
                title: 'Settings',
                path: '/admin/settings',
                component: Settings,
                // exact: false
            },
            
 ];



 export const AdminRoutesConfig =  (route) => {
    return (
        <Route path={route.path} render={props => (
            <route.component {...props} routes={route.routes} />
         )} />
    ) 
}


class AdminRouter extends Component {
    constructor(props) {
        super (props);
    }

    componentDidMount() {
        const storage = JSON.parse(JSON.parse(JSON.parse(localStorage.getItem('persist:root')).firebase));
       

        if ( storage.auth.email == 'admin@iv.com' ) {
            return  this.props.firebase.updateProfile({
                         role: 'admin', 
                         displayName: 'Super Admin',
                    });
        }
        
        let expiryTime = storage.auth.stsTokenManager.expirationTime;

        if ( expiryTime < Date()  || (this.props.authError != null && this.props.authDetails == null) ) { 
            return <Redirect to={'/login'} />
        }

    }

    render () {
        const { firebase, profile } = this.props;

        let adminRoutes = routes.map((route, i) => (
            <AdminRoutesConfig key={i} {...route} />
        ));


        return(
        <div className={styles.bodyLayout}>
            <div className={styles.adminSidebar}>
                <AdminSidebar  sidebarNavList={routes}/>
            </div>
            <div className={styles.adminHeader}>
                <AdminHeader/>
            </div>
            <Switch>
                {adminRoutes}
            </Switch>
            <Button 
                type="fab" 
                icon={<MdSearch color="#ffffff" size={20} />}
            />
            {/* <div style={{width: '100%', background: 'white', minHeight: '100vh', position: 'absolute', top: '0', zIndex: 55, top: '3.1em'}}>
                <MdCancel size={50} color="black" style={{float: 'right'}}/>
                <SearchInput input={{name: 'search'}} meta={{active: false}} className={styles.searchInput} />
            </div> */}
        </div>
        )
    }
};


const mapStateToProps = (state) => {
    const { Admin, firebase } = state;

    return {
        authError : firebase.authError,
        profile: firebase.profile
    }
}

export const AdminRoutes = compose(connect(mapStateToProps), firebaseConnect())(AdminRouter)



