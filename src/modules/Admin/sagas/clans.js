import { call, put, takeEvery, takeLatest } from 'redux-saga/effects'
import { delay } from 'redux-saga'
import { 
    getClans, 
    getFamilies, 
    getUsers, 
    addClan, 
    addFamily, 
    addUser, 
    editClan, 
    editFamily, 
    editUser, 
    deleteUser,
    deleteClan,
    deleteFamily
 } from 'dataSource/api/clans'


/*****      clans       ************ */
 function* fetchClans() {
    yield put({type: "FETCHING_CLANS"})
    try {
        const fetchClans = yield call(getClans);
        const clans = fetchClans.data.response.data
        yield put({type: "FETCHED_CLANS", clans })
        yield put({type: 'FETCH_CLANS_SUCCESS'})
    } catch (error) {
        yield put({ type: 'FETCH_CLANS_ERROR'})
        yield put({type: 'FETCH_CLANS', message: error.message })
    }
 }
 

 export function* watchFetchedClans() {
    yield takeEvery('FETCH_CLANS', fetchClans );
 }


 function* fetchClanDetails() {

 }


 export function* watchFetchClanDetails() {

 }


 function* newClan(){

 }

 export function* watchNewClan() {

 }

 function* updateClan() {

 }

 export function* watchEditClan() {

 }

 function* removeClan() {

 }

 export function* watchRemoveClan() {

 }





/***** Users ********* */

 function* fetchUsers() {

 }

 export function* watchFetchedUsers() {

 }

 function* newUser() {

 }

 function* watchNewUser() {

 }

 function* updateUser() {

 }

 function* watchEditUser() {

 }

 function* removeUser() {

 }

 function* watchRemoveUser() {

 }

/***** Families ********* */
 function* fetchFamilies() {

 }
 export function* watchFetchFamilies() {

 }

 function* newFamilies() {

 }   

 export function* watchNewFamily() {

 }

 export function* watchFetchedFamilies() {

 }

 function* updateFamily() {

 }

 function* watchUpdateFamily() {

 }

 function* removeFamily() {

 }

export function* watchRemoveFamily() {

 }
