import { combineReducers } from 'redux'
import { reducer as FormReducer } from 'redux-form'
import { 
    reactReduxFirebase, 
    firebaseReducer 
} from 'react-redux-firebase'
import AdminReducer from './modules/Admin/moduleReducer'


export const rootReducer =  combineReducers(
    {
        form: FormReducer,  
        firebase: firebaseReducer,
        Admin: AdminReducer,
    }
)

