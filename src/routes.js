import React from 'react'
import {
    BrowserRouter as Router,
    Route,
    Link,
    Switch,
} from 'react-router-dom'
import Admin from './modules/Admin'


const AppRoutes = () => (
    <Router>
        <div>
            <Switch>
                <Route  path="/" component={Admin}/>
            </Switch>
        </div>
    </Router>
)

export default AppRoutes