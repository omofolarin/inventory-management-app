'use strict';

var path = require('path');
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var StatsPlugin = require('stats-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var extractTextPlugin = new ExtractTextPlugin('[name].css');
var glob = require('glob');

extractTextPlugin.options.allChunks = true;
// process.traceDeprecation = true;

module.exports = {
  devtool: 'eval-source-map',
  entry: [
    'webpack-hot-middleware/client?path=/__webpack_hmr&reload=true',
    path.join(__dirname, 'src/main.js')
  ],
  output: {
    path: path.join(__dirname, '/dist/'),
    filename: '[name].js',
    publicPath: '/'
  },
  plugins: [
    new webpack.optimize.OccurrenceOrderPlugin(),
     new HtmlWebpackPlugin({
      template: 'src/index.tpl.html',
      inject: 'body',
      filename: 'index.html'
    }),
     extractTextPlugin,
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': JSON.stringify('development'),
        'API_KEY':  JSON.stringify(process.env.INVENTORY_MANAGEMENT_FIREBASE_API_KEY),
        'DATABASE_URL': JSON.stringify(process.env.INVENTORY_MANAGEMENT_DATABASE_URL),
        'AUTH_DOMAIN': JSON.stringify(process.env.INVENTORY_MANAGEMENT_AUTHDOMAIN),        
        'PROJECT_ID': JSON.stringify(process.env.INVENTORY_MANAGEMENT_PROJECT_ID),
        'STORAGE_BUCKET': JSON.stringify(process.env.INVENTORY_MANAGEMENT_FIREBASE_STORAGE_BUCKET)
      },
    }),
    new StatsPlugin('stats.json', {
      chunkModules: true,
      exclude: [/node_modules/]
    }),
   
  ],
  module: {
    loaders: [{
      test: /\.(jsx|js)?$/,
      exclude: /node_modules/,
      loader: 'babel-loader',
      query: {
        "presets": ["react", "es2015", "stage-0", "react-hmre"]
      }
    }, {
      test: /\.json?$/,
      loader: 'json'
    },
    { test: /\.(css|scss)$/, use: extractTextPlugin.extract([ 'css-loader?sourceMap&modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]!postcss-loader!sass-loader', 'sass-loader'])},

    { test: /\.(gif|png|jpg|jpeg)$/, loader: 'file-loader' },
    {
      test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
      loaders: ["file-loader?limit=10000&mimetype=application/font-woff", 'file' ]
      // loader: 'file',
    },
    {
      test: /\.woff(\?v=\d+\.\d+\.\d+)?$/,
      loader: "file-loader?limit=10000&mimetype=application/font-woff"
    },
    {
      test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/,
      loader: "file-loader?limit=10000&mimetype=application/font-woff"
    },

    {
      test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
      loader: 'file-loader?limit=10000&mimetype=application/octet-stream',
    },
    {
      test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
      loader: 'file-loader?limit=10000&mimetype=image/svg+xml',
    }]
  }
  ,
  resolve: {
     alias: {
      styles: path.resolve(__dirname, './src/assets/styles/'),
      components: path.resolve(__dirname, './src/components/'),
      fonts: path.resolve(__dirname, './src/assets/fonts/'),
      images: path.resolve(__dirname, './src/assets/images/')
    }
  }
};
